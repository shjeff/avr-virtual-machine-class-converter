#pragma once
#include <vector>
#include "defs/javaclass.h"
#include "defs/defines.h"

class SignatureGenerator
{
private:
	static std::vector<signature>* GeneratedSignatures;

	static bool VerifyIfSignatureAlreadyExists(signature generatedSignature)
	{
		for (int i = 0; i < (int)GeneratedSignatures->size(); i++)
			if (GeneratedSignatures->at(i) == generatedSignature)
				return true;
		return false;
	}

	static signature Generate(u4 elementAddress)
	{
		// Tworzenie sygnatury na podstawie adresu w pami�ci
		return (signature)(
			((elementAddress >> 16) & SIGNATURE_DEFAULT_MASK) +
			(elementAddress & SIGNATURE_DEFAULT_MASK) +
			(rand() & SIGNATURE_DEFAULT_MASK));
	}

	static void Correct(signature * generatedSignature)
	{
		u4 generatedSignatureAddress = (u4)(void*)generatedSignature;
		*generatedSignature = *generatedSignature ^ (rand() & SIGNATURE_DEFAULT_MASK) ^
			(generatedSignatureAddress & SIGNATURE_DEFAULT_MASK) ^ (generatedSignatureAddress >> 16);
	}

	static bool MatchesToNativeMethodTable(signature generatedSignature)
	{
		for (int i = 0; i < NATIVE_METHODS_NUM; i++)
			if (g_NativeMethodsTable[i].m_class_signature == generatedSignature || g_NativeMethodsTable[i].m_method_signature == generatedSignature)
				return true;
		return false;
	}

public:
	enum SignatureElement
	{
		SIG_ELEMENT_CLASS,
		SIG_ELEMENT_METHOD_STANDARD,
		SIG_ELEMENT_METHOD_ENTRY_POINT,
		SIG_ELEMENT_METHOD_CLASS_STATIC_INITIALIZER,
		SIG_ELEMENT_METHOD_CLASS_OBJECT_INITIALIZER,
		SIG_ELEMENT_FIELD_STANDARD,
		SIG_ELEMENT_FIELD_STATIC,
	};

	static signature GenerateSignature(SignatureElement signatureType, void * elementAddress)
	{
		u4 elementAddressU4 = (u4)elementAddress;
		signature generatedSignature = Generate(elementAddressU4);
		bool signatureValid = true;
		do
		{
			signatureValid = true;
			switch (signatureType)
			{
			case SignatureGenerator::SIG_ELEMENT_CLASS: break;
			case SignatureGenerator::SIG_ELEMENT_METHOD_STANDARD:
				signatureValid = !((generatedSignature & ~SIGNATURE_METHOD_STANDARD_MASK) >> 14);
				break;
			case SignatureGenerator::SIG_ELEMENT_METHOD_ENTRY_POINT:
				signatureValid = !((generatedSignature & ~SIGNATURE_METHOD_ENTRY_POINT_MASK) >> 14) && (generatedSignature & SIGNATURE_METHOD_ENTRY_POINT);
				break;
			case SignatureGenerator::SIG_ELEMENT_METHOD_CLASS_STATIC_INITIALIZER:
				signatureValid = ((generatedSignature >> 13) == 0x07);
				break;
			case SignatureGenerator::SIG_ELEMENT_METHOD_CLASS_OBJECT_INITIALIZER:
				signatureValid = ((generatedSignature >> 13) == 0x06);
				break;
			case SignatureGenerator::SIG_ELEMENT_FIELD_STANDARD:
				signatureValid = !((generatedSignature & ~SIGNATURE_FIELD_STANDARD_MASK) >> 15);
				break;
			case SignatureGenerator::SIG_ELEMENT_FIELD_STATIC:
				signatureValid = !!(generatedSignature & SIGNATURE_FIELD_STATIC);
				break;
			default:
				break;
			}
			
			if (signatureValid)
				signatureValid = !MatchesToNativeMethodTable(generatedSignature);
			
			if (signatureValid)
				signatureValid = !VerifyIfSignatureAlreadyExists(generatedSignature);
			
			if (!signatureValid)
				Correct(&generatedSignature);
		} while (!signatureValid);

		return generatedSignature;
	}
};

std::vector<signature>* SignatureGenerator::GeneratedSignatures = new std::vector<signature>();