#pragma once
#include <string>
#include "defs/javaclass.h"

class JavaClassMapper
{
private:
	std::string m_fileContent;
	std::string m_outputDirectoryPath;
	bool m_convertedClass;

public:
	JavaClassMapper::JavaClassMapper() { }
	JavaClassMapper::~JavaClassMapper(void) { }

	void JavaClassMapper::CreateStandardClassMap(standard_java_class_file_format stdJavaClass);
	void JavaClassMapper::CreateConvertedClassMap(converted_java_class_file_format convJavaClass);
	
	void JavaClassMapper::DumpMapFile(std::string className);
	void JavaClassMapper::SetOutputDirectoryPath(std::string outputDirectoryPath);
};

