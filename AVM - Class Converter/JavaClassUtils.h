#pragma once
#include "defs/defines.h"
#include "BytecodeReader.h"
#include "JavaClassLoader.h"
#include "Utils.h"


class JavaClassUtils
{
public:

	/// Pobiera ci�g znak�w Utf8 z pola const_pool typu Utf8
	static const char * JavaClassUtils::ReadUtf8(const_pool_info * const_pool)
	{
		if (const_pool->m_tag != CONST_POOL_ID_Utf8)
			return NULL;

		u1* data = const_pool->m_data;
		int size = (int)be_get_u2(data);
		data += 2;
		char * utf8 = new char[size + 1];
		for (int i = 0; i < size; i++)
			utf8[i] = data[i];
		utf8[size] = 0;
		return (const char*)utf8;
	}

	/// Pobiera nazw� atrybutu za pomoc� numeru do pola/litera�u Utf8 b�d�cy nazw� atrybutu
	static const char * JavaClassUtils::GetAttributeNameValue(standard_java_class_file_format* classFormat, int attributeIndex)
	{
		const_pool_info targetConstPool = {};
		GetConstPoolAtIndex(classFormat, &targetConstPool, attributeIndex);
		if (!targetConstPool.m_data)
			return "";
		return ReadUtf8(&targetConstPool);
	}

	/// Pobiera pole/litera� const_pool o numerze index z tabeli litera��w
	static void JavaClassUtils::GetConstPoolAtIndex(standard_java_class_file_format * classFormat, const_pool_info * constPool, int index)
	{
		if (index > classFormat->m_const_pool_count || index < 0)
			return;
		*constPool = classFormat->m_const_pool_table[index - 1];
	}

	static int JavaClassUtils::GetClassStandardFileFormatFromFile(std::string classFilePath, standard_java_class_file_format * classFormat)
	{
		u1* bytecode = 0;
		int bytecodeSize = 0;

		if (!Utils::IsFileExists(classFilePath))
			return 1;

		if (!Utils::IsFileRegular(classFilePath))
			return 1;

		BytecodeReader* bytecodeReader = new BytecodeReader();
		if (bytecodeReader->ReadBytecode(classFilePath) != RES_BYTECODE_READ_OK)
			return 1;
		bytecodeReader->GetBytecode(bytecode, bytecodeSize);

		JavaClassLoader* javaClassLoader = new JavaClassLoader();
		if (javaClassLoader->LoadClass(bytecode, bytecodeSize) != RES_CLASS_LOAD_OK)
			return 1;
		
		*classFormat = javaClassLoader->GetJavaClassFileFormat();
		return RES_CLASS_LOAD_OK;
	}

	static std::vector<std::string> JavaClassUtils::GetClassReferences(standard_java_class_file_format * classFormat)
	{
		std::vector<std::string> vClasses = std::vector<std::string>();

		u2 thisClass = classFormat->m_this_class;
		const_pool_info thisClassConstPool = {};
		GetConstPoolAtIndex(classFormat, &thisClassConstPool, thisClass);

		u2 classUtf8ConstPoolIndex = be_get_u2(thisClassConstPool.m_data);
		const_pool_info utf8ThisClassConstPool = {};
		GetConstPoolAtIndex(classFormat, &utf8ThisClassConstPool, classUtf8ConstPoolIndex);
		std::string thisClassName = STR(ReadUtf8(&utf8ThisClassConstPool));

		for (int i = 0; i < classFormat->m_const_pool_count - 1; i++)
		{
			const_pool_info const_pool = classFormat->m_const_pool_table[i];
			if (const_pool.m_tag == CONST_POOL_ID_Class)
			{
				if ((thisClass - 1) == i)
					continue;

				u1* class_data = const_pool.m_data;
				u2 class_index = be_get_u2(class_data);

				const_pool_info classUtf8 = {};
				GetConstPoolAtIndex(classFormat, &classUtf8, class_index);
				std::string className = ReadUtf8(&classUtf8);
				if (className == "java/lang/Object" || className == "java/lang/String")
					continue;

				vClasses.push_back(className);
			}
		}
		return vClasses;
	}

	static void JavaClassUtils::GetRecursiveClassReferences(std::string rootLibraryPath, std::string className, std::set<std::string> & totalReferences)
	{
		standard_java_class_file_format classFormat = {};
		std::string classFilePath = rootLibraryPath + "/" + className + ".class";
		GetClassStandardFileFormatFromFile(classFilePath, &classFormat);

		totalReferences.insert(className);

		std::vector<std::string> foundClassReferences = GetClassReferences(&classFormat);

		for (int i = 0; i < (int)foundClassReferences.size(); i++)
			totalReferences.insert(foundClassReferences.at(i));

		for (int i = 0; i < (int)foundClassReferences.size(); i++)
		{
			std::string foundClassReference = foundClassReferences.at(i);
			if (totalReferences.find(foundClassReference) == totalReferences.end())
				GetRecursiveClassReferences(rootLibraryPath, foundClassReferences.at(i), totalReferences);
		}
	}

	static u4 JavaClassUtils::GetTypeFlags(char type)
	{
		switch (type)
		{
		case 'D': return TYPE_IS_PRIMITIVE | TYPE_PRIM_IS_NUMERIC | TYPE_NUM_IS_FLOATING | TYPE_NUM_IS_DOUBLE;
		case 'F': return TYPE_IS_PRIMITIVE | TYPE_PRIM_IS_NUMERIC | TYPE_NUM_IS_FLOATING | TYPE_NUM_IS_FLOAT;
		case 'B': return TYPE_IS_PRIMITIVE | TYPE_PRIM_IS_NUMERIC | TYPE_NUM_IS_BYTE;
		case 'C': return TYPE_IS_PRIMITIVE | TYPE_PRIM_IS_NUMERIC | TYPE_NUM_IS_CHAR;
		case 'I': return TYPE_IS_PRIMITIVE | TYPE_PRIM_IS_NUMERIC | TYPE_NUM_IS_INT;
		case 'J': return TYPE_IS_PRIMITIVE | TYPE_PRIM_IS_NUMERIC | TYPE_NUM_IS_LONG;
		case 'S': return TYPE_IS_PRIMITIVE | TYPE_PRIM_IS_NUMERIC | TYPE_NUM_IS_SHORT;
		case 'Z': return TYPE_IS_PRIMITIVE | TYPE_PRIM_IS_BOOLEAN;
		case 'V': return TYPE_IS_PRIMITIVE;
		}
		return 0;
	}
};

