#include "JavaClassReducer.h"
#include <vector>
#include <iostream>
#include <string>
#include <ctime>
#include <set>
#include "SignatureGenerator.h"
#include "JavaClassUtils.h"
#include "JavaClassPrinter.h"
#include "Utils.h"
#include "defs/defines.h"
#include "defs/opcodes.h"

int JavaClassReducer::ReduceClasses()
{
	// Na pocz�tku stw�rz nazwy dla klas i ich sygnatury
	if ((this->m_error = CreateClassSignatures()) != RES_CLASS_REDUCER_OK)
		return this->m_error;

	// Nast�pnie stw�rz sparsowan� skr�con� struktur� klasy
	if ((this->m_error = CreateReducedStructs()) != RES_CLASS_REDUCER_OK)
		return this->m_error;

	// Dokonaj linkowania klas - i ich klas bazowych
	if ((this->m_error = SuperClassSignatureLinking()) != RES_CLASS_REDUCER_OK)
		return this->m_error;

	// Zr�b linkowanie - usu� wpisy Class i Utf8 i zast�p je jednym zmodyfikowanym
	// MethodRef / FieldRef wpisem zawieraj�cym odwo�ania do sygnatur
	if ((this->m_error = LinkMethodImplementation()) != RES_CLASS_REDUCER_OK)
		return this->m_error;

	// Usu� wywo�anie konstruktora z java.lang.Object
	RemoveJavaLangObjectConstructorMethodRef();

	// Usu� te litera�y klas kt�re nie s� u�ywane przez metody (s�owo kluczowe new)
	RemoveUnusedClassConstPools();

	// Modyfikuj tabele litera��w
	ModifyConstPoolTable();

	return RES_CLASS_REDUCER_OK;
}

void JavaClassReducer::RemoveUnusedClassConstPools()
{
	for (int i = 0; i < (int)this->m_userClasses->size(); i++)
	{
		for (int j = 0; j < this->m_userClasses->at(i).m_converted_class_format.m_const_pool_count - 1; j++)
		{
			const_pool_info currentConstPool = this->m_userClasses->at(i).m_converted_class_format.m_const_pool_table[j];
			if (currentConstPool.m_tag == CONST_POOL_ID_Class)
			{
				u2 constPoolNumber = j + 1;
				bool foundCodeCall = false;
				for (int k = 0; k < this->m_userClasses->at(i).m_converted_class_format.m_method_count; k++)
				{
					conv_method_info methodInfo = this->m_userClasses->at(i).m_converted_class_format.m_method_table[k];
					u1* bytecode = methodInfo.m_code;

					for (u4 m = 0; m < methodInfo.m_code_length; m++)
					{
						u1* instruction = bytecode + m;
						if (m < methodInfo.m_code_length - 2)
						{
							if (*instruction == OP_NEW)
							{
								u2 callClassIndex = be_get_u2(instruction + 1);

								if (callClassIndex == constPoolNumber)
								{
									foundCodeCall = true;
									break;
								}
							}
						}
					}
					if (foundCodeCall)
						break;
				}

				if (!foundCodeCall)
				{
					this->m_userClasses->at(i).m_removeConstPoolEntriesSet->insert(constPoolNumber);
				}
			}
		}
	}
}

int JavaClassReducer::CreateClassSignatures()
/// Tworzy nowe zestawy indeks�w do usni�cia. S� to zestawy indeks�w z MethodRef, 
/// neipotrzebnych atrybut�w czy te� odow�ania do java.lang.Object
/// Dodatkowo tworzy sygnatur� dla wszystkich za�adowanych klas, i dodaje
/// litera�y (const_pool) typu Utf8 i Class bie��cej klasy do usuni�cia.
/// Usuwany jest litera� Class i ten kt�ry jest wskazywany przez Class, litera� Utf8
{
	// Stw�rz sygnatury dla klas
	for (int i = 0; i < (int)this->m_userClasses->size(); i++)
	{
		standard_java_class_file_format* currentClass = &this->m_userClasses->at(i).m_class_format;
		signature classSignature = 0;

		// Stw�rz zestaw indeks�w kt�re maj� zosta� usuni�te z const_pool_table
		this->m_userClasses->at(i).m_removeConstPoolEntriesSet = new std::set<u2>();

		this->m_userClasses->at(i).m_removeJavaLangObjectConstPoolEntriesSet = new std::set<u2>();

		// Tworzenie nowej sygnatury dla klasy
		classSignature = SignatureGenerator::GenerateSignature(SignatureGenerator::SignatureElement::SIG_ELEMENT_CLASS, currentClass);

		int thisClassIndex = currentClass->m_this_class;
		const_pool_info thisClassConstPool = {};

		// Pobieranie liera�u na kt�ry wskazuje this_class
		JavaClassUtils::GetConstPoolAtIndex(currentClass, &thisClassConstPool, thisClassIndex);

		if (thisClassConstPool.m_tag != CONST_POOL_ID_Class)
			return RES_CLASS_REDUCER_INVALID_CLASS_FORMAT;

		if (thisClassConstPool.M_size_no_tag == 2)
		{
			u2 utf8ConstPoolIndex = be_get_u2(thisClassConstPool.m_data);
			const_pool_info thisClassUtf8ConstPool = {};

			// Pobieranie litera�u Utf8 na kt�ry wskazuje litera� Class
			JavaClassUtils::GetConstPoolAtIndex(currentClass, &thisClassUtf8ConstPool, utf8ConstPoolIndex);

			if (thisClassUtf8ConstPool.m_tag != CONST_POOL_ID_Utf8)
				return RES_CLASS_REDUCER_INVALID_CLASS_FORMAT;

			// Dodaj indeks litera�u klasy (Utf8) do usuni�cia
			this->m_userClasses->at(i).m_removeConstPoolEntriesSet->insert(utf8ConstPoolIndex);

			// Ustawianie nazwy klasy i sygnatury
			this->m_userClasses->at(i).m_name = JavaClassUtils::ReadUtf8(&thisClassUtf8ConstPool);
			this->m_userClasses->at(i).m_signature = classSignature;
		}
		else {
			return RES_CLASS_REDUCER_INVALID_CLASS_FORMAT;
		}
	}
	return RES_CLASS_REDUCER_OK;
}

int JavaClassReducer::CreateReducedStructs()
/// Tworzy zredukowane struktury klasy. Na pocz�tku tworzy g��wn� struktur�:
/// converted_java_class_file_format, na podstawie istniej�cej:
/// standard_java_class_file_format i kopiuje to converted dane z standard
/// przy czym pomija i nie kopiuje niepotrzebnych danych klasy
/// Tworzy struktury przekszta�conych p�l i metod
{
	for (size_t i = 0; i < this->m_userClasses->size(); i++)
	{
		standard_java_class_file_format* currentClass = &this->m_userClasses->at(i).m_class_format;
		converted_java_class_file_format convertedClass = {};
		convertedClass.m_class_name = this->m_userClasses->at(i).m_name;

		// Modyfikuj pola klasy (uzupe�nainie p�l w przekszta�conej strukturze)
		convertedClass.m_field_count = currentClass->m_field_count;
		convertedClass.m_field_table = new conv_field_info[currentClass->m_field_count];
		if ((this->m_error = ModifyFieldTable(currentClass, convertedClass.m_field_table, i)) != RES_CLASS_REDUCER_OK)
			return this->m_error;

		// Modyfikuj metody klasy (uzupe�nianie metod w przekszta�conej strukturze)
		convertedClass.m_method_count = currentClass->m_method_count;
		std::vector<conv_method_info>* convertedMethods = new std::vector<conv_method_info>();
		
		if ((this->m_error = ModifyMethodTable(currentClass, convertedMethods, i)) != RES_CLASS_REDUCER_OK)
			return this->m_error;

		convertedClass.m_method_table = new conv_method_info[convertedMethods->size()];
		for (int j = 0; j < (int)convertedMethods->size(); j++)
			convertedClass.m_method_table[j] = convertedMethods->at(j);
		convertedClass.m_method_count = convertedMethods->size();

		// Skopiuj const_pool_table
		convertedClass.m_const_pool_count = currentClass->m_const_pool_count;
		convertedClass.m_const_pool_table = new const_pool_info[currentClass->m_const_pool_count];
		for (int j = 0; j < (int)(currentClass->m_const_pool_count - 1); j++)
		{
			// Nowy obszar pami�ci, oddzielny (taki, �e zmienianie nie ma wp�ywu na struktur� standard_java_class_file_format)
			convertedClass.m_const_pool_table[j].m_tag = currentClass->m_const_pool_table[j].m_tag;
			convertedClass.m_const_pool_table[j].M_size_no_tag = currentClass->m_const_pool_table[j].M_size_no_tag;
			convertedClass.m_const_pool_table[j].M_size_total = currentClass->m_const_pool_table[j].M_size_total;
			convertedClass.m_const_pool_table[j].m_data = new u1[currentClass->m_const_pool_table[j].M_size_no_tag];
			for (int k = 0; k < currentClass->m_const_pool_table[j].M_size_no_tag; k++)
				convertedClass.m_const_pool_table[j].m_data[k] = currentClass->m_const_pool_table[j].m_data[k];
		}

		convertedClass.m_this_class_signature = this->m_userClasses->at(i).m_signature;
		this->m_userClasses->at(i).m_converted_class_format = convertedClass;
	}
	return RES_CLASS_REDUCER_OK;
}

int JavaClassReducer::ModifyFieldTable(standard_java_class_file_format* javaClass, conv_field_info* fieldTable, int classIndex)
/// Modyfikuje i wype�nia struktur� przekszta�conych p�l na podstawie standardowej struktury
/// Dodaje do niepotrzebnych indeks�w name_index i attribute_index. Zapisuje deskryptor pola
/// Tworzy sygnatur� dla pola, oraz jego typ
{
	// Stw�rz struktur� przekszta�conych informacji o polach klasy na podstawie standardowych danych
	for (int i = 0; i < javaClass->m_field_count; i++)
	{
		field_info javaField = javaClass->m_field_table[i];
		signature fieldSignature = 0;

		if (javaField.m_access_flags & ACC_STATIC)
			fieldSignature = SignatureGenerator::GenerateSignature(SignatureGenerator::SignatureElement::SIG_ELEMENT_FIELD_STATIC, &javaField);
		else
			fieldSignature = SignatureGenerator::GenerateSignature(SignatureGenerator::SignatureElement::SIG_ELEMENT_FIELD_STANDARD, &javaField);

		conv_field_info fieldInfo = {};
		fieldInfo.m_signature = fieldSignature;

		fieldInfo.m_constant_value_index = 0;
		if (javaField.m_attribute_count)
		{
			for (int i = 0; i < javaField.m_attribute_count; i++)
				if (javaField.m_attribute_table[i].M_attribute_tag == ATTR_FIELD_CONST_VALUE)
					fieldInfo.m_constant_value_index = be_get_u2(javaField.m_attribute_table[i].m_attribute_data);
		}

		u2 fieldDescriptorIndex = javaField.m_descriptor_index;
		u2 fieldNameIndex = javaField.m_name_index;

		const_pool_info fieldDescriptorConstPool = {};
		const_pool_info fieldNameIndexConstPool = {};

		JavaClassUtils::GetConstPoolAtIndex(javaClass, &fieldDescriptorConstPool, fieldDescriptorIndex);
		JavaClassUtils::GetConstPoolAtIndex(javaClass, &fieldNameIndexConstPool, fieldNameIndex);

		if (fieldDescriptorConstPool.m_tag != CONST_POOL_ID_Utf8 || fieldNameIndexConstPool.m_tag != CONST_POOL_ID_Utf8)
			return RES_CLASS_REDUCER_INVALID_CLASS_FORMAT;

		type fieldType = 0;

		std::string fieldDescriptor = STR(JavaClassUtils::ReadUtf8(&fieldDescriptorConstPool));
		std::string fieldName = STR(JavaClassUtils::ReadUtf8(&fieldNameIndexConstPool));

		CreateType(&fieldType, fieldDescriptor);
		fieldInfo.m_type = fieldType;

		fieldInfo.m_ref_details = {};
		fieldInfo.m_ref_details.m_detail_class_name = this->m_userClasses->at(classIndex).m_name;
		fieldInfo.m_ref_details.m_detail_descriptor = fieldDescriptor;
		fieldInfo.m_ref_details.m_detail_name = fieldName;

		fieldTable[i] = fieldInfo;

		// Dodaj indeksy Utf8 do usuni�cia
		this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(javaField.m_name_index);
		this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(javaField.m_descriptor_index);
	}
	return RES_CLASS_REDUCER_OK;
}

int JavaClassReducer::ModifyMethodTable(standard_java_class_file_format* javaClass, std::vector<conv_method_info> * & convertedMethods, int classIndex)
/// Przekszta�ca tabel� metod, tak jak w przypadku p�l, modyfikuje i wype�nia przekszta�con� struktur� metody
/// na podstawie istniej�cej standardowej. Dodaje do niepotrzebnych indeks�w name_index i attribute_index. Zapisuje deskryptor metody.
/// Tworzy sygnatur� dla metody, oraz typ jej parametr�w i warto�ci zwracanej jako u4
{
	for (int i = 0; i < javaClass->m_method_count; i++)
	{
		method_info javaMethod = javaClass->m_method_table[i];

		signature methodSignature = 0;

		conv_method_info methodInfo = {};

		bool foundCodeAttribute = false;

		// Ustaw informacje o implementacji
		for (int j = 0; j < javaMethod.m_attribute_count; j++)
		{
			attribute_info javaMethodAttribute = javaMethod.m_attribute_table[j];
			if (javaMethodAttribute.M_attribute_tag == ATTR_METHOD_CODE)
			{
				foundCodeAttribute = true;
				u1* attributeData = javaMethodAttribute.m_attribute_data;

				// Maksymalna ilo�� element�w na stosie
				be_get_u2_m(methodInfo.m_max_stack, attributeData);

				// Maksymalna ilo�� zmiennych lokalnych
				be_get_u2_m(methodInfo.m_max_locals, attributeData);

				// D�ugo�� bytecodu (implementacji metody)
				be_get_u4_m(methodInfo.m_code_length, attributeData);

				methodInfo.m_code = new u1[methodInfo.m_code_length];
				for (int k = 0; k < (int)methodInfo.m_code_length; k++)
					methodInfo.m_code[k] = *attributeData++;
			}
		}

		if (!foundCodeAttribute)
			return RES_CLASS_REDUCER_INVALID_CLASS_FORMAT;

		const_pool_info methodDescriptorConstPool = {};
		const_pool_info methodNameConstPool = {};

		u2 methodDescriptorIndex = javaMethod.m_descriptor_index;
		u2 methodNameIndex = javaMethod.m_name_index;

		JavaClassUtils::GetConstPoolAtIndex(javaClass, &methodDescriptorConstPool, methodDescriptorIndex);
		JavaClassUtils::GetConstPoolAtIndex(javaClass, &methodNameConstPool, methodNameIndex);

		if (methodDescriptorConstPool.m_tag != CONST_POOL_ID_Utf8 || methodNameConstPool.m_tag != CONST_POOL_ID_Utf8)
			return RES_CLASS_REDUCER_INVALID_CLASS_FORMAT;

		std::string methodDescriptor = JavaClassUtils::ReadUtf8(&methodDescriptorConstPool);
		std::string methodName = JavaClassUtils::ReadUtf8(&methodNameConstPool);

		if (methodName == "<init>" && methodDescriptor == "()V")
		{
			if (methodInfo.m_code_length == 5) {
				u1* code = methodInfo.m_code;
				if (code[0] == OP_ALOAD_0 && code[1] == OP_INVOKESPECIAL && code[4] == OP_RETURN)
				{
					u1* methodRefConstPoolPtr = code + 2;
					u2 methodRefConstPoolIndex = be_get_u2(methodRefConstPoolPtr);
					const_pool_info methodRefConstPool = {};
					JavaClassUtils::GetConstPoolAtIndex(&this->m_userClasses->at(classIndex).m_class_format, &methodRefConstPool, methodRefConstPoolIndex);
					if (methodRefConstPool.m_tag != CONST_POOL_ID_Methodref) {
						break;
					}
					u1* methodRefData = methodRefConstPool.m_data;
					u2 methodRefClassIndex = be_get_u2(methodRefData);
					u2 methodRefNameAndTypeIndex = be_get_u2(methodRefData + 2);
					const_pool_info classConstPool = {};
					JavaClassUtils::GetConstPoolAtIndex(&this->m_userClasses->at(classIndex).m_class_format, &classConstPool, methodRefClassIndex);
					if (classConstPool.m_tag != CONST_POOL_ID_Class) {
						break;
					}
					u1* classData = classConstPool.m_data;
					u2 classUtf8Index = be_get_u2(classData);
					const_pool_info classUtf8ConstPool = {};
					JavaClassUtils::GetConstPoolAtIndex(&this->m_userClasses->at(classIndex).m_class_format, &classUtf8ConstPool, classUtf8Index);
					if (classUtf8ConstPool.m_tag != CONST_POOL_ID_Utf8) {
						break;
					}

					this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(methodRefConstPoolIndex);
					this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(methodRefClassIndex);
					this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(classUtf8Index);

					this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(methodRefNameAndTypeIndex);

					// Dodaj indeksy do usuni�cia z litera�u NameAndType
					const_pool_info nameAndTypeConstPool = {};
					JavaClassUtils::GetConstPoolAtIndex(&this->m_userClasses->at(classIndex).m_class_format, &nameAndTypeConstPool, methodRefNameAndTypeIndex);
					u1* nameAndTypeData = nameAndTypeConstPool.m_data;
					this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(be_get_u2(nameAndTypeData));
					this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(be_get_u2(nameAndTypeData + 2));
					
					if ("java/lang/Object" == STR(JavaClassUtils::ReadUtf8(&classUtf8ConstPool)))
						continue;
				}
			}
		}

		u2 methodAccessFlags = javaMethod.m_access_flags;
		CreateMethodInfoByDescriptor(&methodInfo.m_return_value, methodInfo.m_parameter_table, &methodInfo.m_parameter_count, methodDescriptor);

		if (methodAccessFlags == ENTRY_POINT_METHOD_MODIFIERS && methodName == ENTRY_POINT_METHOD_NAME && methodDescriptor == ENTRY_POINT_METHOD_DESCRIPTOR)
		{
			// Metoda entry point - static void main(String[] args), main:V([Ljava.lang.String;)
			methodSignature = SignatureGenerator::GenerateSignature(SignatureGenerator::SignatureElement::SIG_ELEMENT_METHOD_ENTRY_POINT, &javaMethod);
		}
		else if (methodName == STATIC_INIT_METHOD_NAME)
		{
			// Metoda inicjalizuj�ca kontekst statyczny w klasie
			methodSignature = SignatureGenerator::GenerateSignature(SignatureGenerator::SignatureElement::SIG_ELEMENT_METHOD_CLASS_STATIC_INITIALIZER, &javaMethod);
		}
		else if (methodName == OBJECT_INIT_METHOD_NAME)
		{
			// Metoda inicjalizuj�ca kontekst obiektowy w klasie
			methodSignature = SignatureGenerator::GenerateSignature(SignatureGenerator::SignatureElement::SIG_ELEMENT_METHOD_CLASS_OBJECT_INITIALIZER, &javaMethod);
		}
		else {
			// Zwyk�a metoda
			methodSignature = SignatureGenerator::GenerateSignature(SignatureGenerator::SignatureElement::SIG_ELEMENT_METHOD_STANDARD, &javaMethod);
		}

		methodInfo.m_signature = methodSignature;
		methodInfo.m_ref_details = {};
		methodInfo.m_ref_details.m_detail_class_name = this->m_userClasses->at(classIndex).m_name;
		methodInfo.m_ref_details.m_detail_name = methodName;
		methodInfo.m_ref_details.m_detail_descriptor = methodDescriptor;

		convertedMethods->push_back(methodInfo);

		// Dodaj indeksy Utf8 do usuni�cia
		this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(methodNameIndex);
		this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(methodDescriptorIndex);
	}
	return RES_CLASS_REDUCER_OK;
}

int JavaClassReducer::SuperClassSignatureLinking()
/// Dodaje do super_class do usuni�cia i jego Utf8
{
	for (int i = 0; i < (int)this->m_userClasses->size(); i++)
	{
		java_class_info classInfo = this->m_userClasses->at(i);
		u2 superClassIndex = classInfo.m_class_format.m_super_class;

		const_pool_info superClassConstPool = {};
		JavaClassUtils::GetConstPoolAtIndex(&classInfo.m_class_format, &superClassConstPool, superClassIndex);

		if (superClassConstPool.m_tag != CONST_POOL_ID_Class)
			return RES_CLASS_REDUCER_INVALID_CLASS_FORMAT;

		u2 superClassNameConstPoolIndex = be_get_u2(superClassConstPool.m_data);

		const_pool_info superClassNameConstPool = {};
		JavaClassUtils::GetConstPoolAtIndex(&classInfo.m_class_format, &superClassNameConstPool, superClassNameConstPoolIndex);

		if (superClassNameConstPool.m_tag != CONST_POOL_ID_Utf8)
			return RES_CLASS_REDUCER_INVALID_CLASS_FORMAT;

		std::string superClassName = JavaClassUtils::ReadUtf8(&superClassNameConstPool);

		// Usu� z tabeli litera��w wpis Utf8 odnosz�cy si� do klasy bazowej (zawieraj�cy jej nazw�)
		this->m_userClasses->at(i).m_removeConstPoolEntriesSet->insert(superClassNameConstPoolIndex);

		if (superClassName != "java/lang/Object")
		{
			signature superClassSignature = 0;
			this->GetClassSignature(superClassName, &superClassSignature);

			classInfo.m_converted_class_format.m_super_class_signature = superClassSignature;
		}
	}
	return RES_CLASS_REDUCER_OK;
}

void JavaClassReducer::RemoveJavaLangObjectConstructorMethodRef()
{
	for (int i = 0; i < (int)this->m_userClasses->size(); i++)
	{
		for (int j = 0; j < (int)this->m_userClasses->at(i).m_converted_class_format.m_method_count; j++)
		{
			conv_method_info* method = &this->m_userClasses->at(i).m_converted_class_format.m_method_table[j];
			u1* methodInstructions = method->m_code;

			std::vector<u1>* modifiedMethodImplementation = new std::vector<u1>();
			for (int k = 0; k < (int)method->m_code_length; k++)
			{
				if (*(methodInstructions + k) == OP_ALOAD_0)
				{
					if (k < (int)(method->m_code_length - 3))
					{
						if (*(methodInstructions + 1) == OP_INVOKESPECIAL)
						{
							u2 methodRefIndex = be_get_u2(methodInstructions + k + 2);
							bool foundReference = false;
							for (std::set<u2>::iterator m = this->m_userClasses->at(i).m_removeJavaLangObjectConstPoolEntriesSet->begin();
								m != this->m_userClasses->at(i).m_removeJavaLangObjectConstPoolEntriesSet->end(); m++)
							{
								if (*m == methodRefIndex) {
									foundReference = true;
									break;
								}
							}

							if (foundReference) {
								k += 3;
								continue;
							}
						}
					}
				}

				modifiedMethodImplementation->push_back(*(methodInstructions + k));
			}

			delete[] method->m_code;
			method->m_code = new u1[modifiedMethodImplementation->size()];
			for (int i = 0; i < (int)modifiedMethodImplementation->size(); i++)
				method->m_code[i] = modifiedMethodImplementation->at(i);
			method->m_code_length = modifiedMethodImplementation->size();
		}
	}
}

void JavaClassReducer::CreateType(type* targetType, std::string descriptor)
{
	// Stwarza typ u4 na podstawie deskryptora

	int arrayDimenstion = 0;
	for (int i = 0; i < (int)descriptor.length(); i++)
	{
		// Tablica
		if (descriptor[i] == '[') {
			arrayDimenstion++;
			continue;
		}

		// Typ referencyjny
		if (descriptor[i] == 'L') {
			std::string className = descriptor.substr(i + 1, descriptor.length() - (i + 2));
			signature classSignature = 0;

			if (GetClassSignature(className, &classSignature)) {
				*targetType |= TYPE_REF_CLASS | classSignature;
			}
			break;
		}
		else
		{
			// Typ prymitywny
			*targetType = JavaClassUtils::GetTypeFlags(descriptor[i]);
			break;
		}
	}

	*targetType |= TYPE_PUT_DIMENSION(arrayDimenstion);
}

void JavaClassReducer::CreateMethodInfoByDescriptor(type* returnType, type * & parameterTable, u2* parameterCount, std::string descriptor)
{
	// Stwarza typ u4 dla parametr�w i typu zwracanego metody

	bool returnValueFlag = false;
	int arrayDimension = 0;
	std::vector<type>* paramsVect = new std::vector<type>();

	for (int i = 0; i < (int)descriptor.length(); i++)
	{
		if (!i && descriptor[i] != '(')
			break;

		if (descriptor[i] == ')') {
			returnValueFlag = true;
			continue;
		}

		if (!returnValueFlag)
		{
			if (descriptor[i] == '(')
				continue;

			if (descriptor[i] == '[') {
				arrayDimension++;
				continue;
			}

			type param = 0;

			if (descriptor[i] == 'L') {
				signature classSignature = 0;
				int semicolonIndex = descriptor.find(';', i);
				std::string className = descriptor.substr(i + 1, semicolonIndex - i - 1);

				if (GetClassSignature(className, &classSignature)) {
					param |= TYPE_REF_CLASS | classSignature;
				}

				i = semicolonIndex;
			}
			else {
				param = JavaClassUtils::GetTypeFlags(descriptor[i]);
			}

			param |= TYPE_PUT_DIMENSION(arrayDimension);
			paramsVect->push_back(param);
			arrayDimension = 0;
		}
		else
		{
			std::string returnTypeDescriptor = descriptor.substr(i, descriptor.length() - i);
			CreateType(returnType, returnTypeDescriptor);
			break;
		}
	}

	*parameterCount = paramsVect->size();
	parameterTable = new type[paramsVect->size()];
	for (size_t i = 0; i < paramsVect->size(); i++)
		parameterTable[i] = paramsVect->at(i);
}

void JavaClassReducer::AddClass(standard_java_class_file_format classFormat)
{
	java_class_info classInfo = {};
	classInfo.m_class_format = classFormat;
	this->m_userClasses->push_back(classInfo);
}

void JavaClassReducer::GetReducedClasses(converted_java_class_file_format * & reducedClasses, int * size)
{
	if (!reducedClasses)
	{
		*size = this->m_userClasses->size();
		reducedClasses = new converted_java_class_file_format[this->m_userClasses->size()];
		for (int i = 0; i < *size; i++)
			reducedClasses[i] = this->m_userClasses->at(i).m_converted_class_format;
	}
}

int JavaClassReducer::LinkMethodImplementation()
{
	/// Linkowanie metod

	for (int i = 0; i < (int)this->m_userClasses->size(); i++)
	{
		java_class_info classInfo = this->m_userClasses->at(i);

		std::set<u2>* methodIndexes = new std::set<u2>();
		std::set<u2>* fieldIndexes = new std::set<u2>();
		std::set<u2>* classIndexes = new std::set<u2>();

		for (int j = 0; j < classInfo.m_converted_class_format.m_method_count; j++)
		{
			conv_method_info method = classInfo.m_converted_class_format.m_method_table[j];
			u1* methodInstructions = method.m_code;

			// Implementacja metody (bytecode)
			for (unsigned int k = 0; k < method.m_code_length; k++, methodInstructions++)
			{
				u2 methodIndex = 0, fieldIndex = 0;
				u2 classIndex = 0;

				if (*methodInstructions == OP_INVOKESPECIAL || *methodInstructions == OP_INVOKESTATIC || *methodInstructions == OP_INVOKEVIRTUAL) {
					methodInstructions++;
					methodIndexes->insert(be_get_u2(methodInstructions));
					mov_ptr(methodInstructions, 1);
					k += 2;
				}

				if (*methodInstructions == OP_GETFIELD || *methodInstructions == OP_PUTFIELD || *methodInstructions == OP_GETSTATIC || *methodInstructions == OP_PUTSTATIC) {
					methodInstructions++;
					fieldIndexes->insert(be_get_u2(methodInstructions));
					mov_ptr(methodInstructions, 1);
					k += 2;
				}

				if (*methodInstructions == OP_NEW) {
					methodInstructions++;
					classIndexes->insert(be_get_u2(methodInstructions));
					mov_ptr(methodInstructions, 1);
					k += 2;
				}
			}
		}

		// Na pocz�tku przekszta�� same pola i metody (one odwo�uj� si� do wpis�w klas, je�eli wpisy klas s� przekszta�cone
		// to przekszta�canie metod i p�l zako�czy si� b��dem)
		for (int j = 0; j < classInfo.m_converted_class_format.m_const_pool_count; j++)
		{
			for (std::set<u2>::iterator k = methodIndexes->begin(); k != methodIndexes->end(); k++)
			{
				if ((*k - 1) == j) {
					bool isObjectConstructor = false;
					ConvertMethodRef(&classInfo.m_converted_class_format.m_const_pool_table[j], &classInfo.m_class_format, i, isObjectConstructor);
					if (isObjectConstructor) {
						this->m_userClasses->at(i).m_removeJavaLangObjectConstPoolEntriesSet->insert(*k);
					};
				}
			}

			for (std::set<u2>::iterator k = fieldIndexes->begin(); k != fieldIndexes->end(); k++)
			{
				if ((*k - 1) == j) {
					ConvertFieldRef(&classInfo.m_converted_class_format.m_const_pool_table[j], &classInfo.m_class_format, i);
				}
			}
		}

		for (int j = 0; j < classInfo.m_converted_class_format.m_const_pool_count; j++)
		{
			for (std::set<u2>::iterator k = classIndexes->begin(); k != classIndexes->end(); k++)
			{
				if ((*k - 1) == j) {
					ConvertClassRef(&classInfo.m_converted_class_format.m_const_pool_table[j], &classInfo.m_class_format, i);
				}
			}
		}
	}
	return RES_CLASS_REDUCER_OK;
}

void JavaClassReducer::GetNativeMethodSignature(std::string className, std::string descriptor, std::string name, signature* methodSignature, signature* classSignature)
{
	std::string methodDescriptor = className;
	methodDescriptor.append(".");
	methodDescriptor.append(name);
	methodDescriptor.append(":");
	methodDescriptor.append(descriptor);

	for (int i = 0; i < NATIVE_METHODS_NUM; i++)
	{
		if (g_NativeMethodsTable[i].m_method_desc == methodDescriptor) {
			*classSignature = g_NativeMethodsTable[i].m_class_signature;
			*methodSignature = g_NativeMethodsTable[i].m_method_signature;
			return;
		}
	}
}

bool JavaClassReducer::GetClassSignature(std::string className, signature* classSignature)
{
	for (int i = 0; i < (int)this->m_userClasses->size(); i++) {
		if (this->m_userClasses->at(i).m_name == className) {
			*classSignature = this->m_userClasses->at(i).m_signature;
			return true;
		}
	}
	return false;
}

void JavaClassReducer::GetFieldSignature(std::string className, std::string descriptor, std::string name, signature* fieldSignature, signature* classSignature)
{
	for (int i = 0; i < (int)this->m_userClasses->size(); i++)
	{
		if (this->m_userClasses->at(i).m_name == className) {
			for (int j = 0; j < (int)this->m_userClasses->at(i).m_converted_class_format.m_field_count; j++)
			{
				conv_field_info fieldInfo = this->m_userClasses->at(i).m_converted_class_format.m_field_table[j];
				if (fieldInfo.m_ref_details.m_detail_descriptor == descriptor && fieldInfo.m_ref_details.m_detail_name == name) {
					*fieldSignature = fieldInfo.m_signature;
					*classSignature = this->m_userClasses->at(i).m_converted_class_format.m_this_class_signature;
					return;
				}
			}
		}
	}
}

void JavaClassReducer::GetMethodSignature(std::string className, std::string descriptor, std::string name, signature* methodSignature, signature* classSignature)
{
	for (int i = 0; i < (int)this->m_userClasses->size(); i++)
	{
		if (this->m_userClasses->at(i).m_name == className) {
			for (int j = 0; j < (int)this->m_userClasses->at(i).m_converted_class_format.m_method_count; j++)
			{
				conv_method_info methodInfo = this->m_userClasses->at(i).m_converted_class_format.m_method_table[j];

				if (methodInfo.m_ref_details.m_detail_descriptor == descriptor && methodInfo.m_ref_details.m_detail_name == name)
				{
					*methodSignature = methodInfo.m_signature;
					*classSignature = this->m_userClasses->at(i).m_converted_class_format.m_this_class_signature;
					return;
				}
			}
		}
	}
}

void JavaClassReducer::ConvertMethodRef(const_pool_info* methodRefConstPool, standard_java_class_file_format* javaClass, int classIndex, bool & isObjectConstructor)
{
	// Zamienia wpis MethodRef na taki kt�ry zawiera odwo�ania do metod
	u1* constPoolData = methodRefConstPool->m_data;
	u2 cpClassIndex = 0, nameAndTypeIndex = 0;

	isObjectConstructor = false;

	be_get_u2_m(cpClassIndex, constPoolData);
	be_get_u2_m(nameAndTypeIndex, constPoolData);

	this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(nameAndTypeIndex);

	const_pool_info classConstPool = {};
	const_pool_info nameAndTypeConstPool = {};

	JavaClassUtils::GetConstPoolAtIndex(javaClass, &classConstPool, cpClassIndex);
	JavaClassUtils::GetConstPoolAtIndex(javaClass, &nameAndTypeConstPool, nameAndTypeIndex);

	u2 utf8ClassIndex = be_get_u2(classConstPool.m_data);
	this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(utf8ClassIndex);

	const_pool_info utf8ClassConstPool = {};
	JavaClassUtils::GetConstPoolAtIndex(javaClass, &utf8ClassConstPool, utf8ClassIndex);
	std::string className = JavaClassUtils::ReadUtf8(&utf8ClassConstPool);

	u2 methodNameIndex = 0, methodDescriptorIndex = 0;
	u1* nameAndTypeData = nameAndTypeConstPool.m_data;
	be_get_u2_m(methodNameIndex, nameAndTypeData);
	be_get_u2_m(methodDescriptorIndex, nameAndTypeData);

	this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(methodNameIndex);
	this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(methodDescriptorIndex);

	const_pool_info utf8MethodNameConstPool = {};
	const_pool_info utf8MethodDescriptorConstPool = {};

	JavaClassUtils::GetConstPoolAtIndex(javaClass, &utf8MethodNameConstPool, methodNameIndex);
	JavaClassUtils::GetConstPoolAtIndex(javaClass, &utf8MethodDescriptorConstPool, methodDescriptorIndex);
	std::string methodName = JavaClassUtils::ReadUtf8(&utf8MethodNameConstPool);
	std::string methodDescriptor = JavaClassUtils::ReadUtf8(&utf8MethodDescriptorConstPool);

	signature methodSignature = 0, classSignature = 0;
	GetMethodSignature(className, methodDescriptor, methodName, &methodSignature, &classSignature);

	if (!(methodSignature && classSignature))
	{
		GetNativeMethodSignature(className, methodDescriptor, methodName, &methodSignature, &classSignature);
		if (!(methodSignature && classSignature))
		{
			if (methodName == "<init>" && methodDescriptor == "()V" && className == "java/lang/Object") {
				isObjectConstructor = true;
				return;
			}
		}
	}

	// Stw�rz nowy przekszta�cony wpis MethodRef
	delete[] methodRefConstPool->m_data;
	methodRefConstPool->m_data = new u1[4]
	{
		be_u2_get_st_byte(classSignature), be_u2_get_nd_byte(classSignature),
		be_u2_get_st_byte(methodSignature), be_u2_get_nd_byte(methodSignature)
	};
}

void JavaClassReducer::ConvertClassRef(const_pool_info* classRefConstPool, standard_java_class_file_format* javaClass, int classIndex)
{
	// Zmienia wpis Class na taki kt�ry zawiera odwo�ania do p�l
	u1* constPoolData = classRefConstPool->m_data;
	u2 nameIndex = 0;

	be_get_u2_m(nameIndex, constPoolData);
	this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(nameIndex);

	const_pool_info utf8ClassNameConstPool = {};
	JavaClassUtils::GetConstPoolAtIndex(javaClass, &utf8ClassNameConstPool, nameIndex);
	std::string className = JavaClassUtils::ReadUtf8(&utf8ClassNameConstPool);

	signature classSignature = 0;
	for (int i = 0; i < (int)this->m_userClasses->size(); i++)
	{
		if (this->m_userClasses->at(i).m_name == className) {
			classSignature = this->m_userClasses->at(i).m_signature;
		}
	}

	delete[] classRefConstPool->m_data;
	classRefConstPool->m_data = new u1[2];

	be_u2_get_bytes(classSignature, classRefConstPool->m_data);
}

void JavaClassReducer::ConvertFieldRef(const_pool_info* fieldRefConstPool, standard_java_class_file_format* javaClass, int classIndex)
{
	// Zmienia wpis FieldRef na taki kt�ry zawiera odwo�ania do p�l
	u1* constPoolData = fieldRefConstPool->m_data;
	u2 cpClassIndex = 0, nameAndTypeIndex = 0;

	be_get_u2_m(cpClassIndex, constPoolData);
	be_get_u2_m(nameAndTypeIndex, constPoolData);

	this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(nameAndTypeIndex);

	const_pool_info classConstPool = {};
	const_pool_info nameAndTypeConstPool = {};

	JavaClassUtils::GetConstPoolAtIndex(javaClass, &classConstPool, cpClassIndex);
	JavaClassUtils::GetConstPoolAtIndex(javaClass, &nameAndTypeConstPool, nameAndTypeIndex);

	u2 utf8ClassIndex = be_get_u2(classConstPool.m_data);
	this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(utf8ClassIndex);

	const_pool_info utf8ClassConstPool = {};
	JavaClassUtils::GetConstPoolAtIndex(javaClass, &utf8ClassConstPool, utf8ClassIndex);
	std::string className = JavaClassUtils::ReadUtf8(&utf8ClassConstPool);

	u2 fieldNameIndex = 0, fieldDescriptorIndex = 0;
	u1* nameAndTypeData = nameAndTypeConstPool.m_data;
	be_get_u2_m(fieldNameIndex, nameAndTypeData);
	be_get_u2_m(fieldDescriptorIndex, nameAndTypeData);
	this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(fieldNameIndex);
	this->m_userClasses->at(classIndex).m_removeConstPoolEntriesSet->insert(fieldDescriptorIndex);

	const_pool_info utf8FieldNameConstPool = {};
	const_pool_info utf8FieldDescriptorConstPool = {};

	JavaClassUtils::GetConstPoolAtIndex(javaClass, &utf8FieldNameConstPool, fieldNameIndex);
	JavaClassUtils::GetConstPoolAtIndex(javaClass, &utf8FieldDescriptorConstPool, fieldDescriptorIndex);
	std::string fieldName = JavaClassUtils::ReadUtf8(&utf8FieldNameConstPool);
	std::string fieldDescriptor = JavaClassUtils::ReadUtf8(&utf8FieldDescriptorConstPool);

	signature fieldSignature = 0, classSignature = 0;
	GetFieldSignature(className, fieldDescriptor, fieldName, &fieldSignature, &classSignature);

	// Stw�rz nowy przekszta�cony wpis FieldRef
	delete[] fieldRefConstPool->m_data;
	fieldRefConstPool->m_data = new u1[4]
	{
		be_u2_get_st_byte(classSignature), be_u2_get_nd_byte(classSignature),
		be_u2_get_st_byte(fieldSignature), be_u2_get_nd_byte(fieldSignature)
	};
}

void JavaClassReducer::MakeConstPoolBlank(const_pool_info * constPool)
{
	if (constPool->m_tag & CONST_POOL_ID_SkipPoolFlag)
		return;
	constPool->m_tag |= CONST_POOL_ID_SkipPoolFlag;
	delete[] constPool->m_data;
	constPool->m_data = 0;
	constPool->M_size_total = 1;
	constPool->M_size_no_tag = 0;
}

void JavaClassReducer::ModifyConstPoolTable()
{
	for (int i = 0; i < (int)this->m_userClasses->size(); i++)
	{
		for (int j = 0; j < (int)this->m_userClasses->at(i).m_class_format.m_attribute_count; j++)
		{
			attribute_info attribute = this->m_userClasses->at(i).m_class_format.m_attribute_table[j];
			this->m_userClasses->at(i).m_removeConstPoolEntriesSet->insert(attribute.m_attribute_name_index);

			if (attribute.M_attribute_tag == ATTR_CLASS_SOURCE_FILE) {
				u1* attributeData = attribute.m_attribute_data;
				this->m_userClasses->at(i).m_removeConstPoolEntriesSet->insert(be_get_u2(attributeData));
			}
		}

		for (int j = 0; j < (int)this->m_userClasses->at(i).m_class_format.m_field_count; j++)
		{
			field_info field = this->m_userClasses->at(i).m_class_format.m_field_table[j];
			for (int k = 0; k < (int)field.m_attribute_count; k++)
			{
				attribute_info attribute = this->m_userClasses->at(i).m_class_format.m_field_table[j].m_attribute_table[k];
				this->m_userClasses->at(i).m_removeConstPoolEntriesSet->insert(attribute.m_attribute_name_index);
			}
		}

		for (int j = 0; j < (int)this->m_userClasses->at(i).m_class_format.m_method_count; j++)
		{
			method_info method = this->m_userClasses->at(i).m_class_format.m_method_table[j];
			for (int k = 0; k < (int)method.m_attribute_count; k++)
			{
				attribute_info attribute = this->m_userClasses->at(i).m_class_format.m_method_table[j].m_attribute_table[k];
				this->m_userClasses->at(i).m_removeConstPoolEntriesSet->insert(attribute.m_attribute_name_index);
				if (attribute.M_attribute_tag == ATTR_METHOD_CODE)
				{
					u1* attributeData = attribute.m_attribute_data;

					mov_ptr(attributeData, 4);

					u4 codeLength = 0;
					be_get_u4_m(codeLength, attributeData);
					mov_ptr(attributeData, codeLength);

					mov_ptr(attributeData, 2);
					u2 internalAttributeCount = 0;
					be_get_u2_m(internalAttributeCount, attributeData);

					// Za�o�enie �e exception_table_length zawsze jest 0
					for (int m = 0; m < internalAttributeCount; m++)
					{
						u2 attributeNameIndex = 0;

						be_get_u2_m(attributeNameIndex, attributeData);
						this->m_userClasses->at(i).m_removeConstPoolEntriesSet->insert(attributeNameIndex);

						u4 attributeLength = 0;
						be_get_u4_m(attributeLength, attributeData);

						const_pool_info attributeNameUtf8ConstPool = {};
						JavaClassUtils::GetConstPoolAtIndex(&this->m_userClasses->at(i).m_class_format, &attributeNameUtf8ConstPool, attributeNameIndex);

						std::string attributeNameUtf8 = STR(JavaClassUtils::ReadUtf8(&attributeNameUtf8ConstPool));

						if (attributeNameUtf8 == "LocalVariableTable")
						{
							u2 localVariablesCount = 0;
							be_get_u2_m(localVariablesCount, attributeData);

							for (int p = 0; p < localVariablesCount; p++)
							{
								mov_ptr(attributeData, 4);

								u2 localVariableNameIndex = 0, localVariableDescriptorIndex = 0;
								be_get_u2_m(localVariableNameIndex, attributeData);
								be_get_u2_m(localVariableDescriptorIndex, attributeData);
								mov_ptr(attributeData, 2);
								this->m_userClasses->at(i).m_removeConstPoolEntriesSet->insert(localVariableNameIndex);
								this->m_userClasses->at(i).m_removeConstPoolEntriesSet->insert(localVariableDescriptorIndex);
							}
						}
						else {
							mov_ptr(attributeData, attributeLength);
						}
					}
				}
			}
		}

		int removedConstPoolEntries = 0;
		for (int j = 0; j < this->m_userClasses->at(i).m_converted_class_format.m_const_pool_count; j++)
		{
			for (std::set<u2>::iterator k = this->m_userClasses->at(i).m_removeJavaLangObjectConstPoolEntriesSet->begin();
				k != this->m_userClasses->at(i).m_removeJavaLangObjectConstPoolEntriesSet->end(); k++)
			{
				if ((*k - 1) == j) {
					MakeConstPoolBlank(&this->m_userClasses->at(i).m_converted_class_format.m_const_pool_table[j]);
					removedConstPoolEntries++;
					break;
				}
			}

			for (std::set<u2>::iterator k = this->m_userClasses->at(i).m_removeConstPoolEntriesSet->begin();
				k != this->m_userClasses->at(i).m_removeConstPoolEntriesSet->end(); k++)
			{
				if ((*k - 1) == j) {
					MakeConstPoolBlank(&this->m_userClasses->at(i).m_converted_class_format.m_const_pool_table[j]);
					removedConstPoolEntries++;
					break;
				}
			}
		}
	}
}
