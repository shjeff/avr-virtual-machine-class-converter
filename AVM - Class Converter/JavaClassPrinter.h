#pragma once
#include <iostream>
#include <iomanip>
#include "defs/stdtypes.h"
#include "defs/defines.h"
#include "JavaClassUtils.h"
#include "Utils.h"

class JavaClassPrinter
{
public:
	static void JavaClassPrinter::PrintClassData(standard_java_class_file_format * classFormat)
	{
		std::cout << "Magic number: 0x" << std::hex << std::uppercase << std::setw(8) << classFormat->m_magic_number << std::dec << std::endl;

		std::cout << "Minor version: " << classFormat->m_minor_version << std::endl;
		std::cout << "Major version: " << JavaClassPrinter::GetMajorVersionDesc(classFormat->m_major_version) << std::endl;

		std::cout << "Const pool count: " << classFormat->m_const_pool_count << std::endl;
		std::cout << "Const pool table:" << std::endl;
		PrintConstPoolTable(classFormat->m_const_pool_table, classFormat->m_const_pool_count);

		std::cout << "Access flags: "; JavaClassPrinter::GetAccessFlagsDesc(classFormat->m_access_flags, std::cout); std::cout << std::endl;

		int thisClass = classFormat->m_this_class;
		int superClass = classFormat->m_super_class;
		const_pool_info thisClassConstPool = {};
		const_pool_info superClassConstPool = {};

		JavaClassUtils::GetConstPoolAtIndex(classFormat, &thisClassConstPool, thisClass);
		JavaClassUtils::GetConstPoolAtIndex(classFormat, &superClassConstPool, superClass);

		int thisClassNameIndex = be_get_u2(thisClassConstPool.m_data);
		int superClassNameIndex = be_get_u2(superClassConstPool.m_data);
		const_pool_info thisClassNameConstPool = {};
		const_pool_info superClassNameConstPool = {};

		JavaClassUtils::GetConstPoolAtIndex(classFormat, &thisClassNameConstPool, thisClassNameIndex);
		JavaClassUtils::GetConstPoolAtIndex(classFormat, &superClassNameConstPool, superClassNameIndex);

		const char * thisClassName = JavaClassUtils::ReadUtf8(&thisClassNameConstPool);
		const char * superClassName = JavaClassUtils::ReadUtf8(&superClassNameConstPool);

		std::cout << "This class: #" << (int)thisClass << " (" << thisClassName << ")" << std::endl;
		std::cout << "Super class: #" << (int)superClass << " (" << superClassName << ")" << std::endl;

		std::cout << "Interface count: " << classFormat->m_interface_count << std::endl;
		if (classFormat->m_interface_count) {
			std::cout << "Interfaces: " << std::endl;
			PrintInterfaces(classFormat->m_interface_table, classFormat->m_interface_count);
		}
		std::cout << std::endl;

		std::cout << "Field count: " << classFormat->m_field_count << std::endl;
		if (classFormat->m_field_count) {
			std::cout << "Fields:" << std::endl;
			PrintFields(classFormat->m_field_table, classFormat, classFormat->m_field_count);
		}

		std::cout << "Method count: " << classFormat->m_method_count << std::endl;
		if (classFormat->m_method_count) {
			std::cout << "Methods:" << std::endl;
			PrintMethods(classFormat);
		}
	}

	static void JavaClassPrinter::PrintConvertedClassData(converted_java_class_file_format * convertedClass)
	{
		std::cout << "This class signature: " << Utils::IntToWordHex(convertedClass->m_this_class_signature) << std::endl;
		std::cout << "Super class signature: " << Utils::IntToWordHex(convertedClass->m_this_class_signature) << std::endl;
		
		std::cout << "Const pool count: " << convertedClass->m_const_pool_count << std::endl;
		std::cout << "Const pool table: " << convertedClass->m_const_pool_count << std::endl;
		JavaClassPrinter::PrintConstPoolTable(convertedClass->m_const_pool_table, convertedClass->m_const_pool_count);
		
		std::cout << "Method count: " << convertedClass->m_method_count << std::endl;
		std::cout << "Method table: " << std::endl;
		for (int i = 0; i < convertedClass->m_method_count; i++)
		{
			conv_method_info method = convertedClass->m_method_table[i];
			std::cout << "\tSignature: " << Utils::IntToWordHex(method.m_signature) << std::endl;
			std::cout << "\tReturn type: " << Utils::IntToDoubleWordHex(method.m_return_value) << std::endl;
			int parCount = method.m_parameter_count;
			std::cout << "\tParameter count: " << parCount << std::endl;
			for (int k = 0; k < parCount; k++)
				std::cout << "\t\t" << Utils::IntToDoubleWordHex(method.m_parameter_table[k]) << std::endl;
			std::cout << "\tImplementation: ";
			for (unsigned int i = 0; i < (int)method.m_code_length; i++)
				std::cout << Utils::IntToByteHex(*(method.m_code + i)) << " ";
			std::cout << std::endl;
		}

		std::cout << "Fields: " << convertedClass->m_const_pool_count << std::endl;
		for (int i = 0; i < convertedClass->m_field_count; i++)
		{
			conv_field_info field = convertedClass->m_field_table[i];
			std::cout << "\tSignature: " << Utils::IntToWordHex(field.m_signature) << std::endl;
			std::cout << "\tType: " << Utils::IntToDoubleWordHex(field.m_type) << std::endl;
		}
	}

	static void JavaClassPrinter::PrintMethods(standard_java_class_file_format * classFormat)
	{
		for (int i = 0; i < classFormat->m_method_count; i++)
		{
			method_info method = classFormat->m_method_table[i];
			std::cout << "    Access flags: " << classFormat->m_method_table[i].m_access_flags << std::endl;
			std::cout << "    Name index: #" << classFormat->m_method_table[i].m_name_index << std::endl;
			std::cout << "    Descriptor index: #" << classFormat->m_method_table[i].m_descriptor_index << std::endl;
			std::cout << std::endl;
		}
	}

	static void JavaClassPrinter::PrintFields(field_info* fields, standard_java_class_file_format * classFormat, int fieldCount)
	{
		for (int i = 0; i < fieldCount; i++)
		{
			field_info field = fields[i];
			std::cout << "    Access flags: "; JavaClassPrinter::GetAccessFlagsDesc(field.m_access_flags, std::cout); std::cout << std::endl;
			std::cout << "    Name index: #" << field.m_name_index << std::endl;
			std::cout << "    Descriptor index: #" << field.m_descriptor_index << std::endl;
			int defaultValueIndex = 0;

			if (field.m_attribute_count) {
				u2 attributeNameIndex = field.m_attribute_table[0].m_attribute_name_index;
				if (!(strcmp(JavaClassUtils::GetAttributeNameValue(classFormat, attributeNameIndex), "ConstantValue")))
				{
					u1* attributeBytes = field.m_attribute_table[0].m_attribute_data;
					defaultValueIndex = (int)be_get_u2(attributeBytes);
				}
			}
			std::cout << "    Default value index: #" << defaultValueIndex << std::endl;
			std::cout << std::endl;
		}
	}

	static void JavaClassPrinter::PrintInterfaces(u2* interfaces, int interfaceCount)
	{
		for (int i = 0; i < interfaceCount; i++)
			std::cout << "    #" << (int)interfaces[i] << std::endl;
	}

	static void JavaClassPrinter::PrintConstPoolTable(const_pool_info * constPoolTable, int count)
	{
		std::cout << std::dec;
		for (int i = 0; i < count - 1; i++)
		{
			const_pool_info const_pool = constPoolTable[i];
			u1 tag = const_pool.m_tag;

			std::cout << "    " << (i + 1) << ": " << (!!(tag & CONST_POOL_ID_SkipPoolFlag) ? "\t" : "") << JavaClassPrinter::GetConstPoolName(tag) << std::endl;

			std::cout << "        size: " << const_pool.M_size_total << std::endl;
			u1* bytes = const_pool.m_data;
			std::cout << "        data: ";
			for (int j = 0; j < (int)const_pool.M_size_total - 1; j++)
				std::cout << std::hex << std::uppercase << "0x" << std::setfill('0') << std::setw(2) << (int)bytes[j] << " ";
			std::cout << std::dec;
			if (tag == CONST_POOL_ID_Utf8)
			{
				std::cout << std::endl;
				std::cout << "        raw UTF8: ";
				for (int j = 2; j < (int)const_pool.M_size_total - 1; j++)
					std::cout << std::dec << bytes[j];
			}
			std::cout << std::endl << std::endl;
		}
	}

	/// Pobiera opis wersji
	static const char * JavaClassPrinter::GetMajorVersionDesc(u2 version)
	{
		switch (version)
		{
		case 0x34:
			return "J2SE 8";
		case 0x33:
			return "J2SE 7";
		case 0x32:
			return "J2SE 6.0";
		case 0x31:
			return "J2SE 5.0";
		case 0x30:
			return "JDK 1.4";
		case 0x2F:
			return "JDK 1.3";
		case 0x2E:
			return "JDK 1.2";
		case 0x2D:
			return "JDK 1.1";
		default:
			return "";
		}
	}

	// Pobiera opis flag
	static void JavaClassPrinter::GetAccessFlagsDesc(u2 accessFlags, std::ostream& stream)
	{
		if (accessFlags & ACC_PUBLIC)
			stream << "public ";

		if (accessFlags & ACC_PRIVATE)
			stream << "private ";

		if (accessFlags & ACC_PROTECTED)
			stream << "protected ";

		if (accessFlags & ACC_STATIC)
			stream << "static ";

		if (accessFlags & ACC_FINAL)
			stream << "final ";

		if (accessFlags & ACC_SYNCHRONIZED)
			stream << "synchronized ";

		if (accessFlags & ACC_NATIVE)
			stream << "native ";

		if (accessFlags & ACC_ABSTRACT)
			stream << "abstract ";

		if (accessFlags & ACC_STRICT)
			stream << "strict ";
	}

	/// Pobiera opis pola/litera�u
	static std::string JavaClassPrinter::GetConstPoolName(int tag)
	{
		switch (tag)
		{
		case CONST_POOL_ID_Float:
			return "CONST_POOL_ID_Float";

		case CONST_POOL_ID_Integer:
			return "CONST_POOL_ID_Integer";

		case CONST_POOL_ID_Methodref:
			return "CONST_POOL_ID_Methodref";

		case CONST_POOL_ID_Fieldref:
			return "CONST_POOL_ID_Fieldref";

		case CONST_POOL_ID_InterfaceMethodref:
			return "CONST_POOL_ID_InterfaceMethodref";

		case CONST_POOL_ID_NameAndType:
			return "CONST_POOL_ID_NameAndType";

		case CONST_POOL_ID_Class:
			return "CONST_POOL_ID_Class";

		case CONST_POOL_ID_String:
			return "CONST_POOL_ID_String";

		case CONST_POOL_ID_Long:
			return "CONST_POOL_ID_Long";

		case CONST_POOL_ID_Double:
			return "CONST_POOL_ID_Double";

		case CONST_POOL_ID_Utf8:
			return "CONST_POOL_ID_Utf8";

		default:
			if ((tag & CONST_POOL_ID_SkipPoolFlag) & CONST_POOL_ID_SkipPoolFlag) {
				std::string name = STR(GetConstPoolName(tag & ~CONST_POOL_ID_SkipPoolFlag));
				std::string totalName = "SkipPoolFlag::";
				totalName.append(name);
				totalName.append("");
				return totalName;
			}
			break;
		}
		return "<Unknown name>";
	}
};

