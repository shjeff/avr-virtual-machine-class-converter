#include "JavaClassMapper.h"
#include "Utils.h"
#include "defs/defines.h"
#include <fstream>
#include <string>
#include <iomanip>
#include <sstream>
#include <iostream>

void JavaClassMapper::CreateStandardClassMap(standard_java_class_file_format stdJavaClass)
{
	this->m_convertedClass = false;
	std::string outputFileText = "";

	outputFileText.append("/avm-class-map:v1.0/\n");
	outputFileText.append("\n");
	outputFileText.append("\n");
	outputFileText.append("magic_number: " + Utils::IntToDoubleWordHex(stdJavaClass.m_magic_number) + ";\n");
	outputFileText.append("\n");
	outputFileText.append("minor_version: " + Utils::IntToWordHex(stdJavaClass.m_minor_version) + ";\n");
	outputFileText.append("major_version: " + Utils::IntToWordHex(stdJavaClass.m_major_version) + ";\n");
	outputFileText.append("\n");
	outputFileText.append("const_pool_count: " + Utils::IntToDecStr(stdJavaClass.m_const_pool_count) + ";\n");
	if (stdJavaClass.m_const_pool_count) {
		outputFileText.append("const_pool_table[]:\n");
		outputFileText.append("{\n");

		for (int i = 0; i < stdJavaClass.m_const_pool_count - 1; i++)
		{
			std::string const_pool_bytes = "";
			const_pool_bytes.append(Utils::IntToByteHex(stdJavaClass.m_const_pool_table[i].m_tag) + ((!!stdJavaClass.m_const_pool_table[i].M_size_no_tag) ? ", " : " "));

			for (int j = 0; j < stdJavaClass.m_const_pool_table[i].M_size_no_tag; j++)
				const_pool_bytes.append(Utils::IntToByteHex(stdJavaClass.m_const_pool_table[i].m_data[j]) + ((j < (stdJavaClass.m_const_pool_table[i].M_size_no_tag - 1)) ? ", " : " "));
			outputFileText.append("\tconst_pool#" + Utils::IntToDecStr(i + 1) + ": { " + const_pool_bytes + " };\n");
		}

		outputFileText.append("};\n");
	}
	else {
		outputFileText.append("const_pool_table[]: { };\n");
	}
	outputFileText.append("access_flags: " + Utils::IntToWordHex(stdJavaClass.m_access_flags) + ";\n");
	outputFileText.append("\n");
	outputFileText.append("this_class: #" + Utils::IntToDecStr(stdJavaClass.m_this_class) + ";\n");
	outputFileText.append("super_class: #" + Utils::IntToDecStr(stdJavaClass.m_super_class) + ";\n");
	outputFileText.append("\n");
	outputFileText.append("interface_count: " + Utils::IntToDecStr(stdJavaClass.m_interface_count) + ";\n");

	if (stdJavaClass.m_interface_count) {
		outputFileText.append("interface_table[]:\n");
		outputFileText.append("{\n");
		for (int i = 0; i < stdJavaClass.m_interface_count; i++)
			outputFileText.append("\tinterface: #" + Utils::IntToDecStr(stdJavaClass.m_interface_table[i]) + ";\n");
		outputFileText.append("};\n");
	}
	else {
		outputFileText.append("interface_table[]: { };\n");
	}
	outputFileText.append("\n");
	outputFileText.append("field_count: " + Utils::IntToDecStr(stdJavaClass.m_field_count) + ";\n");
	if (stdJavaClass.m_field_count) {
		outputFileText.append("field_table[]:\n");
		outputFileText.append("{\n");
		for (int i = 0; i < stdJavaClass.m_field_count; i++)
		{
			outputFileText.append("\tfield\n");
			outputFileText.append("\t{\n");
			outputFileText.append("\t\taccess_flags: " + Utils::IntToWordHex(stdJavaClass.m_field_table[i].m_access_flags) + ";\n");
			outputFileText.append("\t\tname_index: #" + Utils::IntToDecStr(stdJavaClass.m_field_table[i].m_name_index) + ";\n");
			outputFileText.append("\t\tdescriptor_index: #" + Utils::IntToDecStr(stdJavaClass.m_field_table[i].m_descriptor_index) + ";\n");
			outputFileText.append("\t\tattribute_count: " + Utils::IntToDecStr(stdJavaClass.m_field_table[i].m_attribute_count) + ";\n");
			if (stdJavaClass.m_field_table[i].m_attribute_count) {
				outputFileText.append("\t\tattribute_table[]:\n");
				outputFileText.append("\t\t{\n");
				for (u4 j = 0; j < stdJavaClass.m_field_table[i].m_attribute_count; j++)
				{
					outputFileText.append("\t\t\tattribute:\n");
					outputFileText.append("\t\t\t{\n");
					outputFileText.append("\t\t\t\tattribute_name_index: #" + Utils::IntToDecStr(stdJavaClass.m_field_table[i].m_attribute_table[j].m_attribute_name_index) + ";\n");
					outputFileText.append("\t\t\t\tattribute_length: " + Utils::IntToDecStr(stdJavaClass.m_field_table[i].m_attribute_table[j].m_attribute_length) + ";\n");
					std::string attr_data = "";
					for (u4 k = 0; k < stdJavaClass.m_field_table[i].m_attribute_table[j].m_attribute_length; k++)
					{
						attr_data.append(Utils::IntToByteHex(stdJavaClass.m_field_table[i].m_attribute_table[j].m_attribute_data[k]) + ((k < (stdJavaClass.m_field_table[i].m_attribute_table[j].m_attribute_length - 1)) ? ", " : " "));
					}
					outputFileText.append("\t\t\t\tattribute_data: { " + attr_data + " } \n");
					outputFileText.append("\t\t\t},\n");
				}
				outputFileText.append("\t\t};\n");
			}
			else {
				outputFileText.append("\t\tattribute_table[]: { };\n");
			}
			outputFileText.append("\t},\n");
		}
		outputFileText.append("};\n");
	}
	else {
		outputFileText.append("field_table[]: { };\n");
	}
	outputFileText.append("\n");
	outputFileText.append("method_count: " + Utils::IntToDecStr(stdJavaClass.m_method_count) + ";\n");
	if (stdJavaClass.m_method_count) {
		outputFileText.append("method_table[]:\n");
		outputFileText.append("{\n");
		for (int i = 0; i < stdJavaClass.m_method_count; i++)
		{
			outputFileText.append("\tmethod\n");
			outputFileText.append("\t{\n");
			outputFileText.append("\t\taccess_flags: " + Utils::IntToWordHex(stdJavaClass.m_method_table[i].m_access_flags) + ";\n");
			outputFileText.append("\t\tname_index: #" + Utils::IntToDecStr(stdJavaClass.m_method_table[i].m_name_index) + ";\n");
			outputFileText.append("\t\tdescriptor_index: #" + Utils::IntToDecStr(stdJavaClass.m_method_table[i].m_descriptor_index) + ";\n");
			outputFileText.append("\t\tattribute_count: " + Utils::IntToDecStr(stdJavaClass.m_method_table[i].m_attribute_count) + ";\n");
			if (stdJavaClass.m_method_table[i].m_attribute_count) {
				outputFileText.append("\t\tattribute_table[]:\n");
				outputFileText.append("\t\t{\n");
				for (u4 j = 0; j < stdJavaClass.m_method_table[i].m_attribute_count; j++)
				{
					outputFileText.append("\t\t\tattribute:\n");
					outputFileText.append("\t\t\t{\n");
					outputFileText.append("\t\t\t\tattribute_name_index: #" + Utils::IntToDecStr(stdJavaClass.m_method_table[i].m_attribute_table[j].m_attribute_name_index) + "\n");
					outputFileText.append("\t\t\t\tattribute_length: #" + Utils::IntToDecStr(stdJavaClass.m_method_table[i].m_attribute_table[j].m_attribute_length) + "\n");
					std::string attr_data = "";
					for (u4 k = 0; k < stdJavaClass.m_method_table[i].m_attribute_table[j].m_attribute_length; k++)
					{
						if (!!!(((k + 1) % 15) || !!!k))
							attr_data.append("\n\t\t\t\t\t\t\t\t");
						attr_data.append(Utils::IntToByteHex(stdJavaClass.m_method_table[i].m_attribute_table[j].m_attribute_data[k]) + ((k < (stdJavaClass.m_method_table[i].m_attribute_table[j].m_attribute_length - 1)) ? ", " : " "));
					}
					outputFileText.append("\t\t\t\tattribute_data: { " + attr_data + " } \n");
					outputFileText.append("\t\t\t},\n");
				}
				outputFileText.append("\t\t};\n");
			}
			else {
				outputFileText.append("\t\tattribute_table[]: { }\n");
			}
			outputFileText.append("\t},\n");
		}
		outputFileText.append("};\n");
	}
	else {
		outputFileText.append("method_table[]: { };\n");
	}
	this->m_fileContent = outputFileText;
}

void JavaClassMapper::CreateConvertedClassMap(converted_java_class_file_format convJavaClass)
{
	std::string outputFileText = "";
	this->m_convertedClass = true;

	outputFileText.append("/avm-class-map:v1.0/\n");
	outputFileText.append("\n");
	outputFileText.append("\n");
	outputFileText.append("class_name: " + convJavaClass.m_class_name + "\n");
	outputFileText.append("\n");
	outputFileText.append("this_class_signature: " + Utils::IntToWordHex(convJavaClass.m_this_class_signature) + ";\n");
	outputFileText.append("super_class_signature: " + Utils::IntToWordHex(convJavaClass.m_super_class_signature) + ";\n");
	outputFileText.append("\n");
	outputFileText.append("const_pool_count: " + Utils::IntToDecStr(convJavaClass.m_const_pool_count) + ";\n");
	if (convJavaClass.m_const_pool_count) {
		outputFileText.append("const_pool_table[]:\n");
		outputFileText.append("{\n");
		for (int i = 0; i < convJavaClass.m_const_pool_count - 1; i++)
		{
			std::string const_pool_bytes = "";
			const_pool_bytes.append(Utils::IntToByteHex(convJavaClass.m_const_pool_table[i].m_tag) + ((!!convJavaClass.m_const_pool_table[i].M_size_no_tag) ? ", " : " "));

			for (int j = 0; j < convJavaClass.m_const_pool_table[i].M_size_no_tag; j++)
				const_pool_bytes.append(Utils::IntToByteHex(convJavaClass.m_const_pool_table[i].m_data[j]) + ((j < (convJavaClass.m_const_pool_table[i].M_size_no_tag - 1)) ? ", " : " "));
			outputFileText.append("\tconst_pool#" + Utils::IntToDecStr(i + 1) + ": { " + const_pool_bytes + " };\n");
		}
		outputFileText.append("};\n");
	}
	else {
		outputFileText.append("const_pool_table[]: { };\n");
	}
	outputFileText.append("\n");
	outputFileText.append("interface_count: " + Utils::IntToDecStr(convJavaClass.m_interface_count) + ";\n");
	if (convJavaClass.m_interface_count) {
		outputFileText.append("interface_table[]:\n");
		outputFileText.append("{\n");
		for (int i = 0; i < convJavaClass.m_interface_count; i++)
			outputFileText.append("\tinterface: #" + Utils::IntToDecStr(convJavaClass.m_interface_table[i]) + ";\n");
		outputFileText.append("};\n");
	}
	else {
		outputFileText.append("interface_table[]: { }\n");
	}
	outputFileText.append("\n");
	outputFileText.append("field_count: " + Utils::IntToDecStr(convJavaClass.m_field_count) + ";\n");
	if (convJavaClass.m_field_count) {
		outputFileText.append("field_table[]:\n");
		outputFileText.append("{\n");
		for (int i = 0; i < convJavaClass.m_field_count; i++)
		{
			outputFileText.append("\tfield\n");
			outputFileText.append("\t{\n");
			outputFileText.append("\t\tsignature: " + Utils::IntToWordHex(convJavaClass.m_field_table[i].m_signature) + ";\n");
			outputFileText.append("\t\tconst_value_index: #" + Utils::IntToDecStr(convJavaClass.m_field_table[i].m_constant_value_index) + ";\n");
			outputFileText.append("\t\ttype: " + Utils::IntToDoubleWordHex(convJavaClass.m_field_table[i].m_type) + ";\n");
			outputFileText.append("\t},\n");
		}
		outputFileText.append("};\n");
	}
	else {
		outputFileText.append("field_table[]: { };\n");
	}
	outputFileText.append("\n");
	outputFileText.append("method_count: " + Utils::IntToDecStr(convJavaClass.m_method_count) + ";\n");
	if (convJavaClass.m_method_count) {
		outputFileText.append("method_table[]:\n");
		outputFileText.append("{\n");
		for (int i = 0; i < convJavaClass.m_method_count; i++)
		{
			outputFileText.append("\tmethod\n");
			outputFileText.append("\t{\n");
			outputFileText.append("\t\tsignature: " + Utils::IntToWordHex(convJavaClass.m_method_table[i].m_signature) + ";\n");
			//outputFileText.append("\t\treturn_type: " + Utils::IntToDoubleWordHex(convJavaClass.m_method_table[i].m_return_value) + ";\n");
			//outputFileText.append("\t\tmax_stack: " + Utils::IntToDecStr(convJavaClass.m_method_table[i].m_max_stack) + ";\n");
			//outputFileText.append("\t\tmax_locals: " + Utils::IntToDecStr(convJavaClass.m_method_table[i].m_max_locals) + ";\n");
			//outputFileText.append("\t\tparameter_count: " + Utils::IntToDecStr(convJavaClass.m_method_table[i].m_parameter_count) + ";\n");
			//if (convJavaClass.m_method_table[i].m_parameter_count) {
			//	outputFileText.append("\t\tparameter_table[]:\n");
			//	outputFileText.append("\t\t{\n");
			//	for (int j = 0; j < convJavaClass.m_method_table[i].m_parameter_count; j++)
			//	{
			//		outputFileText.append("\t\t\tparameter\n");
			//		outputFileText.append("\t\t\t{\n");
			//		outputFileText.append("\t\t\t\ttype: " + Utils::IntToDoubleWordHex(convJavaClass.m_method_table[i].m_parameter_table[j]) + ";\n");
			//		outputFileText.append("\t\t\t},\n");
			//	}
			//	outputFileText.append("\t\t};\n");
			//}
			//else {
			//	outputFileText.append("\t\tparameter_table[]: { };\n");
			//}
			outputFileText.append("\t\tcode_length: " + Utils::IntToDecStr(convJavaClass.m_method_table[i].m_code_length) + ";\n");
			std::string methodCode = "";
			for (u4 k = 0; k < convJavaClass.m_method_table[i].m_code_length; k++)
			{
				if (!!!(((k + 1) % 15) || !!!k))
					methodCode.append("\n\t\t\t\t");
				methodCode.append(Utils::IntToByteHex(convJavaClass.m_method_table[i].m_code[k]) + ((k < (convJavaClass.m_method_table[i].m_code_length - 1)) ? ", " : " "));
			}
			outputFileText.append("\t\tcode: { " + methodCode + "}\n");
			outputFileText.append("\t},\n");
		}
		outputFileText.append("};\n");
	}
	else {
		outputFileText.append("method_table[]: { };\n");
	}

	this->m_fileContent = outputFileText;
}

void JavaClassMapper::DumpMapFile(std::string className)
{
	std::string fileLocation = this->m_outputDirectoryPath + "/Maps";
	std::string fileName = className + (this->m_convertedClass ? "CNV" : "STD") + ".map";
	std::string filePath = fileLocation + "/" + fileName;
	Utils::RecursiveCreateDirectory(fileLocation);
	std::ofstream* outputMapFile = new std::ofstream(filePath, std::ios::out);
	*outputMapFile << this->m_fileContent;
	outputMapFile->close();
}

void JavaClassMapper::SetOutputDirectoryPath(std::string outputDirectoryPath)
{
	this->m_outputDirectoryPath = outputDirectoryPath;
}