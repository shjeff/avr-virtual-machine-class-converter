#include "JavaClassReferenceLoader.h"

bool operator==(const library_class_info& p1, const library_class_info& p2)
{
	// Nadpisz operator == dla por�wnania obiekt�w struktur library_class_info
	return p1.m_class_name == p2.m_class_name && p1.m_class_file_path == p2.m_class_file_path;
}

void JavaClassReferenceLoader::AddUserClass(standard_java_class_file_format * userClass)
{
	this->m_userClasses->push_back(userClass);
}

void JavaClassReferenceLoader::SetLibraryRootPath(std::string javaLibraryRootPath)
{
	this->m_libraryRootPath = javaLibraryRootPath;
}

int JavaClassReferenceLoader::FindReferences(void)
{
	this->m_error = RES_CLASS_REFERENCE_LOADER_OK;

	// Klasy z biblioteki AVR do kt�rych odwo�uje si� pojedyncza klasa u�ytkownika
	std::vector<library_class_info> foundUserClassReferences = std::vector<library_class_info>();

	for (int i = 0; i < (int)this->m_userClasses->size(); i++)
	{
		// Pobieranie referencji (klas kt�rych dana klasa u�ywa)
		std::vector<std::string> userClassReferenceNames = JavaClassUtils::GetClassReferences(this->m_userClasses->at(i));
		for (int j = 0; j < (int)userClassReferenceNames.size(); j++)
		{
			std::string foundClassName = userClassReferenceNames.at(j);
			std::string foundClassPath = this->m_libraryRootPath + "/" + foundClassName + ".class";

			// Dodaj tylko klasy z biblioteki AVR (istniej�ce w lokalizacji m_libraryRootPath)
			if (!Utils::IsFileExists(foundClassPath) || !Utils::IsFileRegular(foundClassPath))
				continue;

			library_class_info libraryClass = {};
			libraryClass.m_class_file_path = foundClassPath;
			libraryClass.m_class_name = foundClassName;
			foundUserClassReferences.push_back(libraryClass);
		}
	}

	// Dla pojedynczej znalezionej klasy z biblioteki, wyszukaj referencyjnie jej odwo�ania do innych klas z biblioteki
	std::vector<library_class_info>* totalLibraryUsedClasses = new std::vector<library_class_info>();
	for (int i = 0; i < (int)foundUserClassReferences.size(); i++)
	{
		totalLibraryUsedClasses->push_back(foundUserClassReferences.at(i));
		std::set<std::string>* foundClassReferences = new std::set<std::string>();
		JavaClassUtils::GetRecursiveClassReferences(this->m_libraryRootPath, foundUserClassReferences.at(i).m_class_name, *foundClassReferences);
		for (std::set<std::string>::iterator k = foundClassReferences->begin();
			k != foundClassReferences->end(); k++)
		{
			library_class_info libClass = {};
			libClass.m_class_file_path = this->m_libraryRootPath + "/" + *k + ".class";
			libClass.m_class_name = *k;
			totalLibraryUsedClasses->push_back(libClass);
		}
	}

	this->m_foundClassReferences->clear();
	for (int i = 0; i < (int)totalLibraryUsedClasses->size(); i++)
	{
		// Je�eli znaleziona klasa z biblioteki jest klas� zawieraj�c� metody natywne, nie uwzgl�dniaj jej
		bool nativeClassFound = false;
		for (int j = 0; j < TOTAL_NATIVE_CLASSES; j++) {
			if (totalLibraryUsedClasses->at(i).m_class_name == g_NativeClassNames[j]) {
				nativeClassFound = true;
				break;
			}
		}

		if (!nativeClassFound && std::find(this->m_foundClassReferences->begin(), this->m_foundClassReferences->end(), totalLibraryUsedClasses->at(i)) == this->m_foundClassReferences->end())
			this->m_foundClassReferences->push_back(totalLibraryUsedClasses->at(i));
	}

	return this->m_error;
}

void JavaClassReferenceLoader::GetFoundReferences(standard_java_class_file_format *& referenceClasses, int & referenceClassesCount)
{
	referenceClasses = new standard_java_class_file_format[this->m_foundClassReferences->size()];
	referenceClassesCount = (int)this->m_foundClassReferences->size();

	for (int i = 0; i < referenceClassesCount; i++)
		JavaClassUtils::GetClassStandardFileFormatFromFile(this->m_foundClassReferences->at(i).m_class_file_path, &referenceClasses[i]);

	this->m_userClasses->clear();
	this->m_foundClassReferences->clear();
}