#include <fstream>
#include <vector>
#include "BytecodeReader.h"
#include "Utils.h"

int BytecodeReader::ReadBytecode(std::string filePath)
{
	this->m_error = RES_BYTECODE_READ_OK;

	if (!Utils::IsFileExists(filePath))
		this->m_error = RES_BYTECODE_READ_FILE_NOT_EXISTS;

	if (!Utils::IsFileRegular(filePath))
		this->m_error = RES_BYTECODE_READ_FILE_IS_NOT_REGULAR;


	if (this->m_error == RES_BYTECODE_READ_OK)
	{
		// Pobieranie rozmiaru pliku (w bajtach)
		int fileSize = Utils::GetFileSize(filePath);

		// Tworzenie obiektu klasy ifstream
		std::ifstream inputFile(filePath, std::ios::binary | std::ios::ate);
		if (inputFile.is_open())
		{
			std::vector<char>* vClassChars = new std::vector<char>();
			inputFile.seekg(0, std::ios::beg);
			char character;

			// Niekt�re bajty mog� mie� warto�� 0xFF a jest to warto�� EOF (End Of File)
			// (dziesi�tnie -1), wi�c nale�y sprawdzi� te� czy wszystkie bajty zosta�y przeczytane
			while ((character = inputFile.get()) != EOF || vClassChars->size() != fileSize) {
				vClassChars->push_back(character);
			}

			if (this->m_classBytecodeCount) {
				delete[] this->m_classBytecode;
			}

			this->m_classBytecodeCount = vClassChars->size();
			this->m_classBytecode = new u1[this->m_classBytecodeCount];

			// Konwersja znak�w w pliku na bajty
			for (int i = 0; i < this->m_classBytecodeCount; i++)
				this->m_classBytecode[i] = (u1)(const char)vClassChars->at(i);
			inputFile.close();

			return RES_BYTECODE_READ_OK;
		}
		else {
			this->m_error = RES_BYTECODE_READ_FILE_IN_USE;
		}
	}

	return this->m_error;
}

int BytecodeReader::GetBytecode(u1 * & bytecode, int & bytecodeSize)
{
	if (this->m_error == RES_BYTECODE_READ_OK)
	{
		// Tworzenie tablicy bajt�w (typ u1) i wpisywanie do niej warto�ci
		bytecode = new u1[this->m_classBytecodeCount];
		for (int i = 0; i < this->m_classBytecodeCount; i++)
			bytecode[i] = this->m_classBytecode[i];

		// Ustawianie rozmiaru
		bytecodeSize = this->m_classBytecodeCount;
	}
	return this->m_error;
}