#pragma once
#include <iostream>
#include <vector>
#include <set>
#include "defs/stdtypes.h"
#include "defs/javaclass.h"

#define RES_CLASS_REDUCER_BASE									(0x50)
#define  RES_CLASS_REDUCER_OK									 (RES_CLASS_REDUCER_BASE|0x00)
#define  RES_CLASS_REDUCER_INVALID_CLASS_FORMAT					 (RES_CLASS_REDUCER_BASE|0x01)

class JavaClassReducer
{
private:
	/* Struktura opisuj�ca za�adowan� klas� */
	struct java_class_info
	{
		std::string m_name;
		signature m_signature;
		standard_java_class_file_format m_class_format;
		converted_java_class_file_format m_converted_class_format;
		std::set<u2>* m_removeConstPoolEntriesSet;
		std::set<u2>* m_removeJavaLangObjectConstPoolEntriesSet;
		std::set<u2>* m_removeClassEntriesSet;
	};

	std::vector<java_class_info>* m_userClasses;						/// Lista za�adowanych klas
	int m_error;														/// Kod b��du

	void JavaClassReducer::GetNativeMethodSignature(std::string className, std::string descriptor, std::string name, signature* methodSignature, signature* classSignature);	/// Pobiera sygnatur� metody natywnej na podstawie jej nazwy, deskryptora i klasy
	void JavaClassReducer::CreateType(type* targetType, std::string descriptor);																								/// Stwrza typ jako liczb� u4 na podstawie deskryptora
	void JavaClassReducer::CreateMethodInfoByDescriptor(type* returnType, type * & parameterTable, u2* parameterCount, std::string descriptor);									/// Stwrza tablic� parametr�w metody i typ zwracany na podstawie jej deskryptora
	bool JavaClassReducer::GetClassSignature(std::string className, signature* classSignature);																					/// Pobiera sygnatur� klasy na podstawie jej nazwy
	void JavaClassReducer::MakeConstPoolBlank(const_pool_info * constPool);																										/// Ustawia najstarszy bit w tagu const_pool informuj�cy o tym, �e te pole ma zosta� pomini�te
	void JavaClassReducer::ModifyConstPoolTable();																																/// Modyfikuje tabel� litera��w (p�l const_pool)
	int  JavaClassReducer::ModifyFieldTable(standard_java_class_file_format* javaClass, conv_field_info* fieldTable, int classIndex);											/// Wype�nia przekszta�cone strukutry p�l
	int  JavaClassReducer::ModifyMethodTable(standard_java_class_file_format* javaClass, std::vector<conv_method_info> * & convertedMethods, int classIndex);					/// Wype�nia przekszta�cone struktury metod
	int  JavaClassReducer::CreateClassSignatures();																																/// Tworzy sygnatury dla wszystkich klas
	int  JavaClassReducer::CreateReducedStructs();																																/// Tworzy przekszta�cone struktury
	int  JavaClassReducer::SuperClassSignatureLinking();																														/// Linkuje poprzez sygnatuyry po��czenia klas z ich klasami bazowymi
	void JavaClassReducer::RemoveJavaLangObjectConstructorMethodRef();																											/// Usuwa metod� (konstruktor) kt�ra tworzy obiekt java.lang.Object
	void JavaClassReducer::RemoveUnusedClassConstPools();																														/// Usuwa indeksy litera�w, kt�re nie s� u�ywane przez kody operacji do tworzenia obiekt�w (OP_NEW)
	void JavaClassReducer::GetFieldSignature(std::string className, std::string descriptor, std::string name, signature* fieldSignature, signature* classSignature);			/// Pobiera sygnatury p�l na podstawie ich nazwy i deskryptora
	void JavaClassReducer::GetMethodSignature(std::string className, std::string descriptor, std::string name, signature* methodSignature, signature* classSignature);			/// Pobiera sygnatury metod na podstawie ich nazwy i deskryptora
	void JavaClassReducer::ConvertMethodRef(const_pool_info* methodRefConstPool, standard_java_class_file_format* javaClass, int classIndex, bool & isObjectConstructor);		/// Konwertuje litera� MethodRef wpisuj�c do niego sygnatury klasy i pola
	void JavaClassReducer::ConvertFieldRef(const_pool_info* fieldRefConstPool, standard_java_class_file_format* javaClass, int classIndex);										/// Konwertuje litera� FieldRef wpisuj�c do niego sygnatury klasy i pola
	void JavaClassReducer::ConvertClassRef(const_pool_info* classRefConstPool, standard_java_class_file_format* javaClass, int classIndex);										/// Konwertuje litra� Class wpisuj�c do niego sygnatur� klasy
	int  JavaClassReducer::LinkMethodImplementation();																															/// Dokonuje linkowania klas, metod i p�l. Zamiena odwo�ania w postaci tekstowej na identyfikatory (sygnatury)


public:
	JavaClassReducer(void) :
		m_userClasses(new std::vector<java_class_info>()),
		m_error(0)
	{ }
	~JavaClassReducer(void) { }

	void JavaClassReducer::AddClass(standard_java_class_file_format classFormat);									/// Dodaje klas� Java
	int JavaClassReducer::ReduceClasses(void);																		/// Usuwa niepotrzebne dane wszystkich za�adowanych klas i ��czy je mi�dzy sob�
	void JavaClassReducer::GetReducedClasses(converted_java_class_file_format * & reducedClasses, int * size);		/// Pobiera przekszta�cone zredukowane klasy
};