#pragma once
#include <ShlObj.h>
#include <iostream>
#include <Windows.h>
#include <string.h>
#include <sstream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <tchar.h>
#include <set>
#include "defs/stdtypes.h"
#include "defs/defines.h"

class Utils
{
private:
	static std::string Utils::ExecutableFilePath;

	static std::vector<std::string> &Split(const std::string &s, char delim, std::vector<std::string> &elems)
	{
		std::stringstream ss(s);
		std::string item;
		while (std::getline(ss, item, delim)) {
			elems.push_back(item);
		}
		return elems;
	}

public:
	Utils()  { }
	~Utils() { }

	static std::set<std::string> RawScanDirectory(std::string directoryPath)
	{
		WIN32_FIND_DATA FindFileData;
		HANDLE hFind;
		std::string location = GetFileLocationFromPath(directoryPath);
		int error = 0;
		if (IsFileRegular(directoryPath))
			return{};

		std::set<std::string> sFoundFiles = std::set<std::string>();
		FindFileData.dwFileAttributes |= FILE_ATTRIBUTE_DIRECTORY;
		hFind = FindFirstFile(directoryPath.c_str(), &FindFileData);
		error = GetLastError();
		while (hFind != INVALID_HANDLE_VALUE && error != ERROR_FILE_NOT_FOUND && error != ERROR_NO_MORE_FILES)
		{
			FindNextFile(hFind, &FindFileData);
			error = GetLastError();

			if (STR(FindFileData.cFileName) == "." || STR(FindFileData.cFileName) == "..")
				continue;

			sFoundFiles.insert(location + '/' + FindFileData.cFileName);
		};
		FindClose(hFind);

		return sFoundFiles;
	}

	static void SetExecutableFilePath(std::string exeFilePath)
	{
		ExecutableFilePath = exeFilePath;
	}

	static std::string GetExecutableFilePath()
	{
		return ExecutableFilePath;
	}

	static std::string GetExeCurrentDirectory()
	{
		char buff[255];
		GetModuleFileName(NULL, buff, 255);
		return Utils::GetFileLocationFromPath(STR(buff));
	}

	static std::vector<std::string> ScanDirectoryRecursive(std::string directoryPath)
	{
		std::string searchExtension = Utils::GetFileExtension(Utils::GetFileNameFromPath(directoryPath));
		std::string directoryLocation = Utils::GetFileLocationFromPath(directoryPath);

		std::set<std::string> sFoundAllFilesInDir = RawScanDirectory(directoryLocation + "/*");
		std::vector<std::string> vFoundFiles = std::vector<std::string>();
		
		if (IsFileRegular(directoryPath) || !sFoundAllFilesInDir.size())
			return vFoundFiles;

		for (std::set<std::string>::iterator i = sFoundAllFilesInDir.begin(); i != sFoundAllFilesInDir.end(); i++)
		{
			std::string filePath = *i;
			if (!IsFileRegular(filePath)) {
				std::string subDirPath = directoryLocation + '/' + Utils::GetFileNameFromPath(filePath) + "/*." + searchExtension;
				std::vector<std::string> subDirFiles = ScanDirectoryRecursive(subDirPath);
				for (size_t j = 0; j < subDirFiles.size(); j++)
					vFoundFiles.push_back(subDirFiles.at(j));
			}
			else {
				std::string f_ext = Utils::GetFileExtension(Utils::GetFileNameFromPath(filePath));
				if (f_ext == searchExtension)
					vFoundFiles.push_back(filePath);
			}
		}

		return vFoundFiles;
	}

	static std::string GetAbsoluteFilePath(const std::string cstrRelativeFilePath)
	{
		char rcBuffer[4096];
		int retVal = GetFullPathName(cstrRelativeFilePath.c_str(), 4096, rcBuffer, 0);
		std::string strAbsolueStr(rcBuffer);
		return strAbsolueStr;
	}

	static bool IsFileRegular(const std::string cstrFilePath)
	{
		DWORD dwFileAttributes = GetFileAttributes(cstrFilePath.c_str());
		return (dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0;
	}

	static bool IsFileExists(const std::string cstrFilePath)
	{
		return (((DWORD)-1) != GetFileAttributes(cstrFilePath.c_str()));
	}

	static std::string IntToByteHex(u1 byte)
	{
		char buffer[5];
		buffer[4] = 0;
		sprintf_s(buffer, "0x%02X", byte);
		return STR(buffer);
	}

	static std::string IntToWordHex(u2 word)
	{
		char buffer[7];
		buffer[6] = 0;
		sprintf_s(buffer, "0x%04X", word);
		return STR(buffer);
	}

	static std::string IntToDoubleWordHex(u4 doubleWord)
	{
		char buffer[11];
		buffer[10] = 0;
		sprintf_s(buffer, "0x%08X", doubleWord);
		return STR(buffer);
	}

	static std::vector<std::string> Split(const std::string &s, char delim)
	{
		std::vector<std::string> elems;
		Split(s, delim, elems);
		return elems;
	}

	static std::string Join(std::vector<std::string> splitted, std::string joinBy)
	{
		std::string joinedStr = "";
		for (int i = 0; i < (int)splitted.size(); i++)
		{
			joinedStr += splitted.at(i);
			if ((i + 1) < (int)splitted.size()) {
				joinedStr += joinBy;
			}
		}
		return joinedStr;
	}

	static std::string GetFileNameFromPath(std::string filePath)
	{
		std::replace(filePath.begin(), filePath.end(), '\\', '/');
		std::vector<std::string> splitted = Split(filePath, '/');
		if (splitted.size() > 0) {
			return splitted.at(splitted.size() - 1);
		}
		return filePath;
	}

	static std::string GetFileLocationFromPath(std::string filePath)
	{
		std::replace(filePath.begin(), filePath.end(), '\\', '/');
		std::vector<std::string> splitted = Split(filePath, '/');
		splitted.pop_back();
		return Join(splitted, "/");
	}

	static std::string GetFileRealName(std::string fileName)
	{
		std::vector<std::string> splitted = Split(fileName, '.');
		if (splitted.size() > 0) {
			splitted.pop_back();
			return Join(splitted, ".");
		}
		return fileName;
	}

	static std::string ToLower(std::string str)
	{
		for (int i = 0; i < (int)str.length(); i++)
			str[i] = tolower(str[i]);
		return str;
	}

	static std::string ToUpper(std::string str)
	{
		for (int i = 0; i < (int)str.length(); i++)
			str[i] = toupper(str[i]);
		return str;
	}

	static std::string GetFileExtension(std::string fileName)
	{
		std::vector<std::string> splitted = Split(fileName, '.');
		if (splitted.size() > 0) {
			return splitted.at(splitted.size() - 1);
		}
		return fileName;
	}

	static std::string IntToDecStr(int number)
	{
		return std::to_string(number);
	}

	static unsigned int GetFileSize(std::string filePath)
	{
		if (!Utils::IsFileExists(filePath) || !Utils::IsFileRegular(filePath))
			return -1;

		std::ifstream file(filePath.c_str(), std::ios::ate | std::ios::binary);
		if (!file.good())
			return -1;
		int fileSize = (unsigned int)file.tellg();
		file.close();
		return fileSize;
	}

	static bool RecursiveCreateDirectory(std::string directoryPath)
	{
		return SHCreateDirectoryEx(NULL, directoryPath.c_str(), NULL) == ERROR_SUCCESS;
	}
};