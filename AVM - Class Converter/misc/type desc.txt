method
-signature:u2
-args:type[]
-return:type

code
-local_variable_count:u2
-operand_stack_count:u2
-bytecode:u1[]


field
-signature:u2
-type:type[]
-constant_value_index:u2 (tylko final)



typedef u4 type_t;
0xAA 0xBB 0xCC 0xDD


0xAA: type classification info (primitive/reference/class/number)
  7654 3210 (bits)
0bAAAA AAAA

+----+--------+--------------+------------+-------------------------+
|bit |    7   |    6...5     |     4      |          3...0          |
+----+--------+--------------+------------+-------------------------+
|desc|   ??   |CLASSIFICATION|IS_PRIMITIVE|TYPE_NAME / SIGNATURE_LOW|
+----+--------+--------------+------------+-------------------------+

	??: unused

	CLASSIFICATION: for primitive is - numeric(01), floating(10) or integral(11),
			for reference - class(01), interface(10) or array(11)

	IS_PRIMITIVE: if 1 type is primitive, if 0 not

0xBB: array dimension: 0-65535


B - 'type' name (float, double)
[BBBB] 0b0000 -> 0b1001