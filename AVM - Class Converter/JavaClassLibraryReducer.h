#pragma once
#include "defs/javaclass.h"

class JavaClassLibraryReducer
{
public:
	JavaClassLibraryReducer(void) { }
	~JavaClassLibraryReducer(void) { }

	void JavaClassLibraryReducer::AddUserClass(standard_java_class_file_format * javaClass);
	void JavaClassLibraryReducer::AddLibraryClass(standard_java_class_file_format * javaClass);
	int JavaClassLibraryReducer::ConvertMethodImplementation();
};

