#pragma once
#include <string>
#include "defs/stdtypes.h"

#define RES_BYTECODE_BASE								(0x10)
#define  RES_BYTECODE_READ_OK							 (RES_BYTECODE_BASE | 0x00)
#define  RES_BYTECODE_READ_FILE_NOT_EXISTS				 (RES_BYTECODE_BASE | 0x01)
#define  RES_BYTECODE_READ_FILE_IS_NOT_REGULAR			 (RES_BYTECODE_BASE | 0x02)
#define  RES_BYTECODE_READ_FILE_IN_USE					 (RES_BYTECODE_BASE | 0x03)

class BytecodeReader
{
private:
	u1* m_classBytecode;									/// Bytecode za�adowanej klasy
	int m_classBytecodeCount;								/// Ilo�� bajt�w (d�ugo�� bytecodu)
	int m_error;											/// Status b��du

public:
	BytecodeReader(void) :
		m_classBytecodeCount(0),
		m_error(0) { }
	~BytecodeReader(void) { }

	int BytecodeReader::ReadBytecode(std::string filePath);						/// �adowanie bytecodu z pliku
	int BytecodeReader::GetBytecode(u1 * & bytecode, int & bytecodeSize);		/// Zwracanie bytecodu
};

