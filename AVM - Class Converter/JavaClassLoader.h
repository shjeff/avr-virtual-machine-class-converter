#pragma once
#include "defs/stdtypes.h"
#include "defs/javaclass.h"

#define RES_CLASS_LOAD_BASE								(0x20)
#define  RES_CLASS_LOAD_OK								 (RES_CLASS_LOAD_BASE|0x00)
#define  RES_CLASS_LOAD_MAJOR_VERSION_INCORRECT			 (RES_CLASS_LOAD_BASE|0x01)
#define  RES_CLASS_LOAD_MAGIC_NUMBER_INCORRECT			 (RES_CLASS_LOAD_BASE|0x02)
#define  RES_CLASS_LOAD_CONST_POOL_SIZE_TOO_SMALL		 (RES_CLASS_LOAD_BASE|0x03)
#define  RES_CLASS_LOAD_UNKNOWN_ATTRIBUTE_NAME			 (RES_CLASS_LOAD_BASE|0x04)

class JavaClassLoader
{
private:
	standard_java_class_file_format m_classFileFormat;		/// Struktura klasy
	u1* m_classBytecode;									/// Kod bytecode za�adowanej klasy
	int m_classBytecodeCount;								/// Ilo�� bajt�w (d�ugo�� bytecodu)
	int m_error;

	int  JavaClassLoader::LoadConstPoolTable(u1 * & pByteCodePosition);									/// Parsowanie tabeli zawieraj�cej litera�y
	int  JavaClassLoader::LoadConstPool(const_pool_info * constPool, u1 * & pByteCodePosition);			/// Parsowanie pojedynczego litera�u
	int  JavaClassLoader::GetConstPoolSize(int constPoolTag);											/// Pobieranie d�ugo�ci litera�u
	int  JavaClassLoader::GetUtf8ConstPoolSize(u1 * pUtf8ConstPoolStartPosition);						/// Pobieranie d�ugo�ci litera�u UTF8 (zmienna warto��)
	int  JavaClassLoader::LoadInterfaceTable(u1 * & pByteCodePosition);									/// Parsowanie interfejs�w
	int  JavaClassLoader::LoadFieldTable(u1 * & pByteCodePosition);										/// Parsowanie p�l w klasie
	int  JavaClassLoader::LoadMethodTable(u1 * & pByteCodePosition);									/// Parsowanie metod klasy
	int  JavaClassLoader::LoadAttributes(u1 * & pByteCodePosition);										/// Parsowanie atrybut�w klasy
	void JavaClassLoader::GetAttributeType(u2 attributeNameIndex, u1 & attributeTag);					/// Konwersja nazwy atrybutu na tag int
	void JavaClassLoader::GetJavaClassName(void);														/// Pobieranie nazwy klasy
	int  JavaClassLoader::LoadInternalAttributes(u1 * & pByteCodePosition, attribute_info * attribute);	/// Parsowanie atrybut�w struktur metod lub p�l
	void JavaClassLoader::ParseClass(void);																/// Parsowanie ca�ej klasy

public:
	JavaClassLoader(void):
		m_classBytecodeCount(0),
		m_error(0) { }
	~JavaClassLoader(void) { }

	int JavaClassLoader::LoadClass(u1 * bytecode, int bytecodeSize);			/// Parsowanie klasy (tworzenie struktury) z bytecode
	standard_java_class_file_format JavaClassLoader::GetJavaClassFileFormat();	/// Pobieranie sparsowanego formatu klasy Java jako struktury
};
