#pragma once
#include <iostream>

#define CONSOLE_ARG_MODE_CONVERT_CLASSES			0x01
#define  CONSOLE_ARG_OPTION_VERBOSE					 0x01
#define  CONSOLE_ARG_OPTION_CREATE_MAP_FILES		 0x02

#define CONSOLE_ARG_MODE_SHOW_CLASS_INFO			0x02
#define  CONSOLE_ARG_OPTION_SHOW_BYTECODE			 0x01

#define CONSOLE_ARG_MODE_CREATE_CLASS_MAP			0x03
#define  CONSOLE_ARG_OPTION_MAP_DIFF				 0x01

#define CONSOLE_ARG_MODE_SHOW_HELP					0x04

#define RES_CONSOLE_ARG_BASE									(0x30)
#define  RES_CONSOLE_ARG_PARSE_OK								 (RES_CONSOLE_ARG_BASE|0x00)
#define  RES_CONSOLE_ARG_PARSE_ERROR_INPUT_FILE_DOESNT_EXISTS	 (RES_CONSOLE_ARG_BASE|0x01)
#define  RES_CONSOLE_ARG_PARSE_ERROR_INPUT_FILE_IS_REGULAR		 (RES_CONSOLE_ARG_BASE|0x02)
#define  RES_CONSOLE_ARG_PARSE_ERROR_INPUT_FILE_IS_DIRECTORY	 (RES_CONSOLE_ARG_BASE|0x03)
#define  RES_CONSOLE_ARG_PARSE_ERROR_UNKNOWN_OPTION				 (RES_CONSOLE_ARG_BASE|0x04)
#define  RES_CONSOLE_ARG_PARSE_ERROR_MISSING_PARAMETERS			 (RES_CONSOLE_ARG_BASE|0x05)


class ConsoleArguments
{
private:
	std::ostream& m_outStream;								/// Strumień wyjściowy
	char** m_argv;											/// Argumenty programu
	int m_argc;												/// Ilość argumentów
	int m_runMode;											/// Tryb programu
	int m_optionNumber;										/// Dodatkowe opcje
	bool m_parametersValid;
	std::string m_rootUserClassesDirectoryPath;				/// Tryb konwersji klas - lokalizacja folderu zawierającego skompilowane klasy użytkownika
	std::string m_rootLibraryClassesDirectoryPath;			/// Tryb konwersji klas - lokalizacja folderu zaeierającego bibliotekę Java AVR
	std::string m_classFilePath;							/// Tryb pokazywania informacji o klasie - lokalizacja skompilowanej klasy Java (plik .class)
	std::string m_classFilePathA;
	std::string m_classFilePathB;

public:
	ConsoleArguments::ConsoleArguments(int argc, char** argv, std::ostream& out_stream) :
		m_argc(argc),
		m_argv(argv),
		m_runMode(0),
		m_optionNumber(0),
		m_rootUserClassesDirectoryPath(""),
		m_rootLibraryClassesDirectoryPath(""),
		m_classFilePath(""),
		m_classFilePathA(""),
		m_classFilePathB(""),
		m_parametersValid(false),
		m_outStream(out_stream) {  }

	ConsoleArguments::~ConsoleArguments(void) {}

	int ConsoleArguments::ParseArgs(void);								/// Parsowanie argumentów programu
	void ConsoleArguments::PrintHelp(void);								/// Wyświetlanie pomocy
	void ConsoleArguments::PrintInfo(void);								/// Wyświetlanie informacji i programie
	bool ConsoleArguments::ArgsExists(void);							/// Sprawdzenie czy program został wywołany z argumentami
	int ConsoleArguments::GetMode(void);								/// Pobieranie trybu pracy programu
	std::string ConsoleArguments::MapGetInputFileA(void);
	std::string ConsoleArguments::MapGetInputFileB(void);
	std::string ConsoleArguments::InfoGetInputFile(void);
	std::string ConsoleArguments::ParseGetSourceDirectory(void);
	std::string ConsoleArguments::ParseGetLibDirectory(void);
	std::string ConsoleArguments::ErrorInfo(int error);					/// Zamiana kodu błędu na treść
	void ConsoleArguments::PrintCommandLine(void);						/// Wyświetlanie wpisanych argumentów do programu
	int ConsoleArguments::GetOptionNumber(void);						/// Pobieranie dotakowych opcji programu
};

