#include <string>
#include "Utils.h"
#include "JavaClassLoader.h"
#include "defs/defines.h"
#include "JavaClassUtils.h"

int JavaClassLoader::LoadConstPoolTable(u1 * & pByteCodePosition)
{
	// Parsowanie tabeli litera��w
	this->m_classFileFormat.m_const_pool_table = new const_pool_info[this->m_classFileFormat.m_const_pool_count];
	
	for (int i = 0; i < (int)this->m_classFileFormat.m_const_pool_count - 1; i++)
	{
		this->m_classFileFormat.m_const_pool_table[i] = {};

		// Parsowanie pojedynczego pola
		if ((this->m_error = this->LoadConstPool(&this->m_classFileFormat.m_const_pool_table[i], pByteCodePosition)) != RES_CLASS_LOAD_OK)
			break;
	}

	return this->m_error;
}


int JavaClassLoader::LoadConstPool(const_pool_info * constPool, u1 * & pByteCodePosition)
{
	// Ustaw bajt tagu - typu pola
	constPool->m_tag = *pByteCodePosition;
	int currentConstPoolSize = 0;

	// Je�eli typ pola to Utf8 pobierz d�ugo�� tego pola
	if (constPool->m_tag == CONST_POOL_ID_Utf8) {
		currentConstPoolSize = this->GetUtf8ConstPoolSize(pByteCodePosition);
	}
	else {
		currentConstPoolSize = this->GetConstPoolSize(constPool->m_tag);
	} pByteCodePosition++;

	// Minimalna d�ugo�� litera�u to 3 bajty (UTF8 ma minimum 4 (u1, u2, u1[]))
	if (currentConstPoolSize < 3)
		return RES_CLASS_LOAD_CONST_POOL_SIZE_TOO_SMALL;

	constPool->M_size_total = currentConstPoolSize;
	constPool->M_size_no_tag = currentConstPoolSize - 1;

	// Parsowanie danych pola
	constPool->m_data = new u1[currentConstPoolSize - 1];
	for (int i = 0; i < currentConstPoolSize - 1; i++)
		constPool->m_data[i] = *pByteCodePosition++;

	return RES_CLASS_LOAD_OK;
}

int JavaClassLoader::GetConstPoolSize(int constPoolTag)
{
	// Pobieranie d�ugo�ci sta�ego pola
	switch (constPoolTag)
	{
	case CONST_POOL_ID_Float:
	case CONST_POOL_ID_Integer:
	case CONST_POOL_ID_Methodref:
	case CONST_POOL_ID_Fieldref:
	case CONST_POOL_ID_InterfaceMethodref:
	case CONST_POOL_ID_NameAndType:
		return 5;
	case CONST_POOL_ID_Class:
	case CONST_POOL_ID_String:
		return 3;
	case CONST_POOL_ID_Long:
	case CONST_POOL_ID_Double:
		return 9;
	}
	return -1;
}

int JavaClassLoader::GetUtf8ConstPoolSize(u1 * pUtf8ConstPoolStartPosition)
{
	// UTF8: u1 tag; u2 length; u1[] utf8data;
	// Pierwszy bajt (u1) to jest typ pola, kolejne dwa bajty to d�ugo�� pola
	// zatem ca�kowita d�ugo�� to od pozycji kt�ra wskazuje na typ pola to
	// przesuni�cie o 1 bajt tak, �eby wskazywa�o na d�ugo�� pola jako warto�� u2
	// i to jest d�ugo�� utf8 ale trzeba doda� jeszcze 3 bajty z kt�rych w�a�nie
	// dwa s� na d�ugo�� utf8 i jeden na typ pola, �e typ pola to UTF8
	return 3 + be_get_u2(1 + pUtf8ConstPoolStartPosition);
}

int JavaClassLoader::LoadInterfaceTable(u1 * & pByteCodePosition)
{
	// Parsowanie interfejs�w
	if (this->m_classFileFormat.m_interface_count)
	{
		this->m_classFileFormat.m_interface_table = new u2[this->m_classFileFormat.m_interface_count];
		for (int i = 0; i < (int)this->m_classFileFormat.m_interface_count; i++)
			be_get_u2_m(this->m_classFileFormat.m_interface_table[i], pByteCodePosition);
	}
	else {
		this->m_classFileFormat.m_interface_table = 0;
	}
	return RES_CLASS_LOAD_OK;
}

int JavaClassLoader::LoadFieldTable(u1 * & pByteCodePosition)
{
	// Parsowanie p�l w klasie
	if (this->m_classFileFormat.m_field_count)
	{
		this->m_classFileFormat.m_field_table = new field_info[this->m_classFileFormat.m_field_count];
		for (int i = 0; i < (int)this->m_classFileFormat.m_field_count; i++)
		{
			this->m_classFileFormat.m_field_table[i] = {};

			// Flagi dost�pu pola
			be_get_u2_m(this->m_classFileFormat.m_field_table[i].m_access_flags, pByteCodePosition);

			// Indeks nazwy
			be_get_u2_m(this->m_classFileFormat.m_field_table[i].m_name_index, pByteCodePosition);

			// Indeks deskryptora
			be_get_u2_m(this->m_classFileFormat.m_field_table[i].m_descriptor_index, pByteCodePosition);

			// Ilo�� atrybut�w
			int attributeCount = this->m_classFileFormat.m_field_table[i].m_attribute_count = be_get_u2(pByteCodePosition);
			mov_ptr(pByteCodePosition, 2);

			// Parsowanie wewn�trznych atrybut�w pola
			if (attributeCount) {
				this->m_classFileFormat.m_field_table[i].m_attribute_table = new attribute_info[attributeCount];

				for (int j = 0; j < attributeCount; j++)
				{
					this->m_classFileFormat.m_field_table[i].m_attribute_table[j] = {};
					if ((this->LoadInternalAttributes(pByteCodePosition, &this->m_classFileFormat.m_field_table[i].m_attribute_table[j])) != RES_CLASS_LOAD_OK)
						return this->m_error;
				}
			}
			else {
				this->m_classFileFormat.m_field_table[i].m_attribute_table = 0;
			}
		}
	}
	else {
		this->m_classFileFormat.m_field_table = 0;
	}
	return RES_CLASS_LOAD_OK;
}


int JavaClassLoader::LoadMethodTable(u1 * & pByteCodePosition)
{
	if (this->m_classFileFormat.m_method_count)
	{
		// Parsowanie metod w klasie
		this->m_classFileFormat.m_method_table = new method_info[this->m_classFileFormat.m_method_count];
		for (int i = 0; i < (int)this->m_classFileFormat.m_method_count; i++)
		{
			this->m_classFileFormat.m_method_table[i] = {};

			// Flagi dost�pu metody
			be_get_u2_m(this->m_classFileFormat.m_method_table[i].m_access_flags, pByteCodePosition);

			// Indeks nazwy
			be_get_u2_m(this->m_classFileFormat.m_method_table[i].m_name_index, pByteCodePosition);

			// Indeks deskryptora
			be_get_u2_m(this->m_classFileFormat.m_method_table[i].m_descriptor_index, pByteCodePosition);

			// Ilo�� atrybut�w
			int attributeCount = this->m_classFileFormat.m_method_table[i].m_attribute_count = be_get_u2(pByteCodePosition);
			mov_ptr(pByteCodePosition, 2);

			// Parsowanie wewn�trznych atrybut�w metody
			if (attributeCount) {
				this->m_classFileFormat.m_method_table[i].m_attribute_table = new attribute_info[attributeCount];

				for (int j = 0; j < attributeCount; j++)
				{
					this->m_classFileFormat.m_method_table[i].m_attribute_table[j] = {};
					if ((this->LoadInternalAttributes(pByteCodePosition, &this->m_classFileFormat.m_method_table[i].m_attribute_table[j])) != RES_CLASS_LOAD_OK)
						return this->m_error;
				}
			}
			else {
				this->m_classFileFormat.m_method_table[i].m_attribute_table = 0;
			}
		}
	}
	else {
		this->m_classFileFormat.m_method_table = 0;
	}
	return RES_CLASS_LOAD_OK;
}

int JavaClassLoader::LoadAttributes(u1 * & pByteCodePosition)
{
	// Parsowanie atrybut�w klasy
	if (this->m_classFileFormat.m_attribute_count)
	{
		this->m_classFileFormat.m_attribute_table = new attribute_info[this->m_classFileFormat.m_attribute_count];

		for (int i = 0; i < (int)this->m_classFileFormat.m_attribute_count; i++)
		{
			int attributesOffset = 0;
			this->m_classFileFormat.m_attribute_table[i] = {};
			if (this->LoadInternalAttributes(pByteCodePosition, &this->m_classFileFormat.m_attribute_table[i]))
				return this->m_error;
		}
	}
	else {
		this->m_classFileFormat.m_attribute_table = 0;
	}
	return RES_CLASS_LOAD_OK;
}

void JavaClassLoader::GetAttributeType(u2 attributeNameIndex, u1 & attributeTag)
{
	// Pobieranie litera�u zawieraj�cego nazw� atrybutu
	const_pool_info attribute_name_const_pool = {};
	std::string attributeName = STR(JavaClassUtils::GetAttributeNameValue(&this->m_classFileFormat, attributeNameIndex));

	if (attributeName == "Code") {
		attributeTag = ATTR_METHOD_CODE;
	}
	else if (attributeName == "ConstantValue") {
		attributeTag = ATTR_FIELD_CONST_VALUE;
	}
	else if (attributeName == "SourceFile") {
		attributeTag = ATTR_CLASS_SOURCE_FILE;
	}
	else if (attributeName == "InnerClasses") {
		attributeTag = ATTR_CLASS_INNER_CLASSES;
	}
	else if (attributeName == "LocalVariableTable") {
		attributeTag = ATTR_METHOD_LOCAL_VARIABLE_TABLE;
	}
	else if (attributeName == "LineNumberTable") {
		attributeTag = ATTR_METHOD_LINE_NUMBER_TABLE;
	}
	else {
		attributeTag = ATTR_UNKNOWN;
	}
}

int JavaClassLoader::LoadInternalAttributes(u1 * & pByteCodePosition, attribute_info * attribute)
{
	// Indeks nazwy atrybutu
	be_get_u2_m(attribute->m_attribute_name_index, pByteCodePosition);

	// D�ugo�� atrybutu
	be_get_u4_m(attribute->m_attribute_length, pByteCodePosition);

	// Pobierz typ atrybutu jako int
	this->GetAttributeType(attribute->m_attribute_name_index, attribute->M_attribute_tag);
	if (attribute->M_attribute_tag == ATTR_UNKNOWN)
		return this->m_error = RES_CLASS_LOAD_UNKNOWN_ATTRIBUTE_NAME;

	// Dane atrybutu
	attribute->m_attribute_data = new u1[attribute->m_attribute_length];
	for (int i = 0; i < (int)attribute->m_attribute_length; i++)
		attribute->m_attribute_data[i] = *pByteCodePosition++;

	return RES_CLASS_LOAD_OK;
}

void JavaClassLoader::GetJavaClassName(void)
{
	const_pool_info thisClassConstPool = {}, utf8ThisClassConstPool = {};

	JavaClassUtils::GetConstPoolAtIndex(&this->m_classFileFormat, &thisClassConstPool, this->m_classFileFormat.m_this_class);
	u2 ut8ThisClassConstPoolIndex = be_get_u2(thisClassConstPool.m_data);

	JavaClassUtils::GetConstPoolAtIndex(&this->m_classFileFormat, &utf8ThisClassConstPool, ut8ThisClassConstPoolIndex);
	this->m_classFileFormat.m_class_name = STR(JavaClassUtils::ReadUtf8(&utf8ThisClassConstPool));
}

void JavaClassLoader::ParseClass(void)
{
	// Resetowanie b��du
	this->m_error = RES_CLASS_LOAD_OK;
	
	u1* pByteCodePosition = this->m_classBytecode;

	// Liczba kontrolna 0xCAFEBABE
	if ((this->m_classFileFormat.m_magic_number = be_get_u4(pByteCodePosition)) != 0xCAFEBABE) {
		this->m_error = RES_CLASS_LOAD_MAGIC_NUMBER_INCORRECT;
		return;
	} mov_ptr(pByteCodePosition, 4);

	// Wersja
	be_get_u2_m(this->m_classFileFormat.m_minor_version, pByteCodePosition);

	u2 majorVersion = be_get_u2(pByteCodePosition);
	if (majorVersion < 0x2D || majorVersion > 0x34) {
		this->m_error = RES_CLASS_LOAD_MAJOR_VERSION_INCORRECT;
		return;
	} mov_ptr(pByteCodePosition, 2);
	this->m_classFileFormat.m_major_version = majorVersion;

	// Ilo�� litera��w
	be_get_u2_m(this->m_classFileFormat.m_const_pool_count, pByteCodePosition);

	// Parsowanie litera��w
	if ((this->m_error = this->LoadConstPoolTable(pByteCodePosition)) != RES_CLASS_LOAD_OK)
		return;

	// Flagi dost�pu
	be_get_u2_m(this->m_classFileFormat.m_access_flags, pByteCodePosition);

	// Klasa this
	be_get_u2_m(this->m_classFileFormat.m_this_class, pByteCodePosition);
	this->GetJavaClassName();


	// Klasa super
	be_get_u2_m(this->m_classFileFormat.m_super_class, pByteCodePosition);
	
	// Ilo�� Interfejs�w
	be_get_u2_m(this->m_classFileFormat.m_interface_count, pByteCodePosition);

	// Parsowanie interfejs�w
	this->LoadInterfaceTable(pByteCodePosition);

	// Ilo�� p�l
	be_get_u2_m(this->m_classFileFormat.m_field_count, pByteCodePosition);

	// Parsowanie p�l
	if ((this->m_error = this->LoadFieldTable(pByteCodePosition)) != RES_CLASS_LOAD_OK)
		return;

	// Ilo�� metod
	be_get_u2_m(this->m_classFileFormat.m_method_count, pByteCodePosition);

	// Parsowanie metod
	if ((this->m_error = this->LoadMethodTable(pByteCodePosition)) != RES_CLASS_LOAD_OK)
		return;

	// Ilo�� atrybut�w (klasy)
	be_get_u2_m(this->m_classFileFormat.m_attribute_count, pByteCodePosition);

	// Parsowanie atrybut�w
	this->m_error = this->LoadAttributes(pByteCodePosition);
}

int JavaClassLoader::LoadClass(u1 * bytecode, int bytecodeSize)
{
	// Prze�adowanie tablicy
	if (this->m_classBytecodeCount) {
		delete[] this->m_classBytecode;
	}

	this->m_classBytecodeCount = bytecodeSize;
	this->m_classBytecode = new u1[bytecodeSize];
	for (int i = 0; i < bytecodeSize; i++)
		this->m_classBytecode[i] = bytecode[i];

	// Parsowanie klasy
	this->ParseClass();

	return this->m_error;
}

standard_java_class_file_format JavaClassLoader::GetJavaClassFileFormat()
{
	return this->m_classFileFormat;
}