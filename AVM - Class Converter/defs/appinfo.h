#pragma once

#define PROG_NAME "avm-cc"
#define PROG_SHORT_DESCRIPTION "AVR Virtual Machine Class Converter"
#define PROG_DETAILED_DESCRIPTION (\
		"This program parses compiled class files and removes unnecessary library\n" \
		"classes that only refers to classes with native methods. This outputs a\n" \
		"C header .h file/files that contain class without any refers to intermediate\n" \
		"library classes, but with reference to native methods.")
#define PROG_AUTHOR "shjeff"
#define PROG_VERSION "v1.0.0.0"
#define PROG_LEGAL "Copyright (c) 2015 - 2016"
