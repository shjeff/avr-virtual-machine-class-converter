#pragma once

#define OP_INVOKEDYNAMIC		0xBA
#define OP_INVOKEINTERFACE		0xB9

#define OP_INVOKESPECIAL		0xB7
#define OP_INVOKESTATIC			0xB8
#define OP_INVOKEVIRTUAL		0xB6

#define OP_GETFIELD				0xB4
#define OP_PUTFIELD				0xB5
#define OP_GETSTATIC			0xB2
#define OP_PUTSTATIC			0xB3

#define OP_NEW					0xBB

#define OP_ALOAD_0				0x2A
#define OP_RETURN				0xB1
