#pragma once
#include "javaclass.h"

#define STR(pcstr)							std::string(pcstr)

/// Tagi const pool - tabeli sta�ych warto�ci
#define CONST_POOL_ID_Utf8					1
#define CONST_POOL_ID_Integer				3
#define CONST_POOL_ID_Float					4
#define CONST_POOL_ID_Long					5
#define CONST_POOL_ID_Double				6
#define CONST_POOL_ID_Class					7
#define CONST_POOL_ID_String				8

#define CONST_POOL_ID_Fieldref				9
#define CONST_POOL_ID_Methodref				10
#define CONST_POOL_ID_InterfaceMethodref	11
#define CONST_POOL_ID_NameAndType			12

#define CONST_POOL_ID_NativeMethod			64
#define CONST_POOL_ID_SkipPoolFlag			128

/// Flagi dost�pu (do metod i p�l klasy)
#define ACC_PUBLIC							0x0001
#define ACC_PRIVATE							0x0002
#define ACC_PROTECTED						0x0004
#define ACC_STATIC							0x0008
#define ACC_FINAL							0x0010
#define ACC_SYNCHRONIZED					0x0020
#define ACC_NATIVE							0x0100
#define ACC_ABSTRACT						0x0400
#define ACC_STRICT							0x0800

/// Tagi atrybut�w (w�asna implementacja)
#define ATTR_CLASS_SOURCE_FILE					0x01
#define ATTR_FIELD_CONST_VALUE					0x02
#define ATTR_METHOD_CODE						0x03
#define ATTR_METHOD_LINE_NUMBER_TABLE			0x04
#define ATTR_METHOD_LOCAL_VARIABLE_TABLE		0x05
#define ATTR_CLASS_INNER_CLASSES				0x06
#define ATTR_UNKNOWN							0x07

/// |   bajt 1   |    bajt 2   |   bajt 3   |   bajt 4    |
/// | ?abb cccc  |  dddd ????  | eeee eeee  |  eeee eeee  |
/// | 7654 3210  |  7654 3210  | 7654 3210  |  7654 3210  |
//
// a - czy typ jest prymitywny (1 tak, 0 oznacza referencyjny)
// b - klasyfikacja typu (prymitywy: 00 - void, 01 - return address, 10 - boolean, 11 - numeric, referencje: 01 - class, 10 - interface, 11 - array)
// c - oznaczenie typu prymitywnego (prymitywy: 1xxx - floating, 0xxx - integral)
// d - wymiary tablicy 1 -> [], 2 -> [][], (je�eli 0 to znaczy, �e to nie tablica)
// e - sygnatura klasy/interfejsu (je�eli typ jest referencj� do klasy lub interfejsu)
// ? - nieu�yte

#define TYPE_IS_PRIMITIVE			(0x40 << 24) // *1** ****
#define  TYPE_PRIM_IS_RET_ADDR		(0x50 << 24) // *101 ****
#define  TYPE_PRIM_IS_BOOLEAN		(0x60 << 24) // *110 ****
#define  TYPE_PRIM_IS_NUMERIC		(0x70 << 24) // *111 ****
#define   TYPE_NUM_IS_FLOATING		(0x08 << 24) // **** 1***
#define     TYPE_NUM_IS_FLOAT		(0x09 << 24) // **** 1001
#define     TYPE_NUM_IS_DOUBLE		(0x0A << 24) // **** 1010
#define   TYPE_NUM_IS_BYTE			(0x01 << 24) // **** 0001
#define   TYPE_NUM_IS_SHORT			(0x02 << 24) // **** 0010
#define   TYPE_NUM_IS_INT			(0x03 << 24) // **** 0011
#define   TYPE_NUM_IS_LONG			(0x04 << 24) // **** 0100
#define   TYPE_NUM_IS_CHAR			(0x05 << 24) // **** 0101

#define  TYPE_REF_CLASS				(0x10 << 24) // *001 ****
#define  TYPE_REF_INTERFACE			(0x20 << 24) // *010 ****
#define  TYPE_REF_ARRAY				(0x30 << 24) // *011 ****

#define TYPE_PUT_DIMENSION(d)		(((d)& 0x0F) << 20)

#define SIGNATURE_DEFAULT_MASK		0xFFFF

#define SIGNATURE_METHOD_INIT				(1uL << 13)
#define SIGNATURE_METHOD_NATIVE				(1uL << 14)
#define SIGNATURE_METHOD_ENTRY_POINT		(1uL << 15)

#define SIGNATURE_FIELD_STATIC				(1uL << 15)

#define SIGNATURE_CLASS_STANDARD_MASK		0xFFFF

#define SIGNATURE_FIELD_STANDARD_MASK		0x7FFF
#define SIGNATURE_FIELD_STATIC_MASK			0xFFFF

#define SIGNATURE_METHOD_STANDARD_MASK		0x3FFF // 00AA AAAA BBBB BBBB /~00AA = 1100
#define SIGNATURE_METHOD_ENTRY_POINT_MASK	0xBFFF // 10AA AAAA BBBB BBBB /~10AA = 0100
#define SIGNATURE_METHOD_NATIVE_MASK		0x7FFF // 01AA AAAA BBBB BBBB /~01AA = 1000
#define SIGNATURE_METHOD_INIT_STATIC_MASK	0xFFFF // 111A AAAA BBBB BBBB /~111A = 0000
#define SIGNATURE_METHOD_INIT_OBJECT_MASK	0xDFFF // 110A AAAA BBBB BBBB /~110A = 0010



#define GET_SIGNATURE(s, m, e, n)	(signature)((m) ? ((e) ? (s | SIGNATURE_METHOD_ENTRY_POINT) : (s & ~SIGNATURE_METHOD_ENTRY_POINT)) : (s))

#define MAKE_BYTE(d)				(u1)d
#define MAKE_WORD(d)				(u2)d
#define MAKE_DWORD(d)				(u4)d
#define MAKE_INT(d)					(int)d

#define ENTRY_POINT_METHOD_MODIFIERS		0x09
#define ENTRY_POINT_METHOD_NAME				STR("main")
#define ENTRY_POINT_METHOD_DESCRIPTOR		STR("([Ljava/lang/String;)V")

#define STATIC_INIT_METHOD_NAME				STR("<clinit>")

#define OBJECT_INIT_METHOD_NAME				STR("<init>")

struct native_method_descriptor
{
	native_method_descriptor(const std::string _method_desc, signature _class_signature, signature _method_signature) :
		m_method_desc(_method_desc),
		m_class_signature(_class_signature),
		m_method_signature(_method_signature){}

	const std::string  m_method_desc;
	const signature    m_class_signature;
	const signature    m_method_signature;
};

#define NATIVE_CLASS_REGISTRY_SIGNATURE					0x1000
#define  NATIVE_METHOD_REGISTRY_SETBIT_SIGNATURE			SIGNATURE_METHOD_NATIVE| 0x1001
#define  NATIVE_METHOD_REGISTRY_CLEARBIT_SIGNATURE			SIGNATURE_METHOD_NATIVE| 0x1002
#define  NATIVE_METHOD_REGISTRY_GET_SIGNATURE				SIGNATURE_METHOD_NATIVE| 0x1003
#define  NATIVE_METHOD_REGISTRY_SET_SIGNATURE				SIGNATURE_METHOD_NATIVE| 0x1004
#define  NATIVE_METHOD_REGISTRY_GETBIT_SIGNATURE			SIGNATURE_METHOD_NATIVE| 0x1005
#define  NATIVE_METHOD_REGISTRY_TOGGLEBIT_SIGNATURE			SIGNATURE_METHOD_NATIVE| 0x1006
#define  NATIVE_METHOD_REGISTRY_SETBITSTATE_SIGNATURE		SIGNATURE_METHOD_NATIVE| 0x1007

#define NATIVE_CLASS_DELAY_SIGNATURE					0x2000
#define  NATIVE_METHOD_DELAY_SEC							SIGNATURE_METHOD_NATIVE| 0x2001
#define  NATIVE_METHOD_DELAY_MSEC							SIGNATURE_METHOD_NATIVE| 0x2002
#define  NATIVE_METHOD_DELAY_USEC							SIGNATURE_METHOD_NATIVE| 0x2003

#define NATIVE_METHODS_NUM								10

const struct native_method_descriptor g_NativeMethodsTable[NATIVE_METHODS_NUM] =
{
	native_method_descriptor("avr/base/Registry.setBit:(II)V",        NATIVE_CLASS_REGISTRY_SIGNATURE, NATIVE_METHOD_REGISTRY_SETBIT_SIGNATURE),
	native_method_descriptor("avr/base/Registry.clearBit:(II)V",      NATIVE_CLASS_REGISTRY_SIGNATURE, NATIVE_METHOD_REGISTRY_CLEARBIT_SIGNATURE),
	native_method_descriptor("avr/base/Registry.get:(I)I",            NATIVE_CLASS_REGISTRY_SIGNATURE, NATIVE_METHOD_REGISTRY_GET_SIGNATURE),
	native_method_descriptor("avr/base/Registry.set:(II)V",           NATIVE_CLASS_REGISTRY_SIGNATURE, NATIVE_METHOD_REGISTRY_SET_SIGNATURE),
	native_method_descriptor("avr/base/Registry.getBit:(II)I",        NATIVE_CLASS_REGISTRY_SIGNATURE, NATIVE_METHOD_REGISTRY_GETBIT_SIGNATURE),
	native_method_descriptor("avr/base/Registry.toggleBit:(II)V",     NATIVE_CLASS_REGISTRY_SIGNATURE, NATIVE_METHOD_REGISTRY_TOGGLEBIT_SIGNATURE),
	native_method_descriptor("avr/base/Registry.setBitState:(III)V",  NATIVE_CLASS_REGISTRY_SIGNATURE, NATIVE_METHOD_REGISTRY_SETBITSTATE_SIGNATURE),

	native_method_descriptor("avr/util/Delay.sec:(I)V",               NATIVE_CLASS_DELAY_SIGNATURE, NATIVE_METHOD_DELAY_SEC),
	native_method_descriptor("avr/util/Delay.msec:(I)V",              NATIVE_CLASS_DELAY_SIGNATURE, NATIVE_METHOD_DELAY_MSEC),
	native_method_descriptor("avr/util/Delay.usec:(I)V",              NATIVE_CLASS_DELAY_SIGNATURE, NATIVE_METHOD_DELAY_USEC),
};

#define NATIVE_CLASS_REGISTRY_NAME						STR("avr/base/Registry")
#define NATIVE_CLASS_DELAY_NAME							STR("avr/util/Delay")

#define TOTAL_NATIVE_CLASSES							2

static std::string g_NativeClassNames[TOTAL_NATIVE_CLASSES] = 
{
	NATIVE_CLASS_REGISTRY_NAME,
	NATIVE_CLASS_DELAY_NAME
};