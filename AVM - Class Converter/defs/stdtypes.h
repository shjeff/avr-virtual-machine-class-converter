#pragma once
#include <inttypes.h>

/// Aliasy typ�w (bajt, s�owo, podw�jne s�owo)
typedef uint8_t			u1;
typedef uint16_t		u2;
typedef uint32_t		u4;

// Maksymalna wato�� z typu nieoznaczonego u1, u2 lub u4
#define u1_max_value				((u1)-1)
#define u2_max_value				((u2)-1)
#define u4_max_value				((u4)-1)

#define mov_ptr(p, o)				((p)+=(o))

/// Pobieranie z tablicy danych WORD (u2) lub DWORD (u4)
#define	be_get_u2(p)				(u2)((((*((p)))& 0xFF)<<8)|((*((p)+1))& 0xFF))
#define be_get_u4(p)				(u4)((((*((p)))& 0xFF)<<24)|(((*((p)+1))& 0xFF)<<16)|(((*((p)+2))&0xFF)<<8)|((*((p)+3))&0xFF))

/// Tak samo jak wy�ej pobiera z tablicy bajty i pakuje do WORD lub DWORD, ale z dodatkowym przesuni�ciem wska�nika
#define	be_get_u2_m(_v, _p) \
		(_v) = be_get_u2(_p);	\
		mov_ptr(_p, 2)

#define be_get_u4_m(_v, _p)	\
		(_v) = be_get_u4(_p);	\
		mov_ptr(_p, 4)

/// Wpisywanie do tablicy danych WORD lub DWORD
#define be_u2_get_bytes(u, p) \
	*((p))		= (((u) >>  8) & 0xFF); \
	*((p) + 1)	= ((u)         & 0xFF);

#define be_u4_get_bytes(u, p) \
	*((p))		= (((u) >> 24) & 0xFF); \
	*((p) + 1)	= (((u) >> 16) & 0xFF); \
	*((p) + 2)	= (((u) >>  8) & 0xFF); \
	*((p) + 3)	= ((u)         & 0xFF);

/// Pobieranie pojedynczych bajt�w z danej WORD lub DWORD
#define be_u2_get_st_byte(u)		(u1)((((u))>>8)& 0xFF)
#define be_u2_get_nd_byte(u)		(u1)(((u))& 0xFF)

#define be_u4_get_st_byte(u)		(u1)((((u))>>24)& 0xFF)
#define be_u4_get_nd_byte(u)		(u1)((((u))>>16)& 0xFF)
#define be_u4_get_rd_byte(u)		(u1)((((u))>>8)& 0xFF)
#define be_u4_get_th_byte(u)		(u1)(((u))& 0xFF)