#pragma once
#include "stdtypes.h"
#include <string>

struct const_pool_info
{
	u1  m_tag;								// Oznaczenie litera�u
	u1* m_data;								// Dane litera�u

	u2  M_size_total;						// Rozmiar ca�ego pola (��cznie z oznaczeniem m_tag)
	u2  M_size_no_tag;						// Rozmiar pola bez tag
};

struct attribute_info
{
	u2  m_attribute_name_index;				// Indeks do litera�u - nazwy atrybutu

	u4  m_attribute_length;					// D�ugo�� atrybutu
	u1* m_attribute_data;					// Dane atrybutu

	u2  M_size_total;						// Rozmiar ca�ego atrybutu
	u1  M_attribute_tag;					// Oznaczenie atrybutu
};

struct field_info
{
	u2  m_access_flags;						// Flagi dost�pu pola
	u2  m_name_index;						// Indeks do litera�u - nazwy pola
	u2  m_descriptor_index;					// Indeks do litera�u - deskryptora pola

	u2  m_attribute_count;					// Ilo�� atrybut�w pola
	attribute_info* m_attribute_table;		// Dane atrybut�w pola
};

struct method_info
{
	u2  m_access_flags;						// Flagi dost�pu do metody
	u2  m_name_index;						// Indeks do litera�u - nazwy metody
	u2  m_descriptor_index;					// Indeks do litera�u - deskryptora metody

	u2  m_attribute_count;					// Ilo�� atrybut�w metody
	attribute_info* m_attribute_table;		// Dane atrybut�w metody
};

struct standard_java_class_file_format
{
	u4  m_magic_number;						// Numer kontrolny

	u2  m_minor_version;					// Wersja minor
	u2  m_major_version;					// Wersja major

	u2  m_const_pool_count;					// Ilo�� litera��w
	const_pool_info* m_const_pool_table;	// Tablica litera��w

	u2  m_access_flags;						// Flagi dost�pu

	u2  m_this_class;						// Indeks do klasy
	u2  m_super_class;						// Indeks do klasy bazowej

	u2  m_interface_count;					// Ilo�� interfejs�w
	u2* m_interface_table;					// Tablica interfejs�w - indeks�w litera��w

	u2  m_field_count;						// Ilo�� p�l w klasie
	field_info* m_field_table;				// Tablica p�l

	u2  m_method_count;						// Ilo�� metod w klasie
	method_info* m_method_table;			// Tablica metod

	u2  m_attribute_count;					// Ilo�� atrybut�w klasy
	attribute_info* m_attribute_table;		// Tablica atrybut�w klasy

	std::string m_class_name;				// Nazwa klasy
};

typedef u4 type;							// Typ pola lub parametr�w / warto�ci zwracanej z metody
/// |   bajt 1   |    bajt 2   |   bajt 3   |   bajt 4    |
/// | ?abb cccc  |  dddd ????  | eeee eeee  |  eeee eeee  |
/// | 7654 3210  |  7654 3210  | 7654 3210  |  7654 3210  |
//
// a - czy typ jest prymitywny
// b - klasyfikacja typu
// c - wymiary tablicy 1 -> [], 2 -> [][], (je�eli 0 to znaczy, �e to nie tablica)
// d - oznaczenie typu prymitywnego
// e - sygnatura klasy (je�eli typ jest referencj� do klasy)
// ? - nieu�yte

typedef u2 signature;

struct reference_info_details
{
	std::string m_detail_class_name;
	std::string m_detail_name;
	std::string m_detail_descriptor;
};

struct conv_method_info
{
	signature m_signature;					// Sygnatura metody np. 0x49E5

	u2  m_parameter_count;					// Ilo�� parametr�w metody
	type* m_parameter_table;				// Tablica typ�w parametr�w

	type m_return_value;					// Typ warto�ci zwracanej

	u2  m_max_locals;						// Maksymalny rozmiar tablicy lokalnych zmiennych
	u2  m_max_stack;						// Maksymalny rozmiar stosu operand�w

	u4  m_code_length;						// D�ugo�� kodu (implementacji metody)
	u1* m_code;								// Implementacja metody (kod bytecode)

	reference_info_details m_ref_details;	// Szczeg�y (potrzebne tylko w przetwarzaniu)
};

struct conv_field_info
{
	signature m_signature;					// Sygnatura pola

	type m_type;							// Typ pola

	u2  m_constant_value_index;				// Opcjonalna sta�a warto��

	reference_info_details m_ref_details;	// Szczeg�y (potrzebne tylko w przetwarzaniu)
};

struct converted_java_class_file_format
{
	u2  m_const_pool_count;					// Ilo�� litera��w
	const_pool_info* m_const_pool_table;	// const_pool jest taki sam jak w klasie standardowej

	signature m_this_class_signature;		// Sygnatura klasy
	signature m_super_class_signature;		// Sygnatura klasy bazowej

	u2  m_interface_count;					// Ilo�� interfejs�w implementowanych przez klas�
	u2* m_interface_table;					// Interfejsy implementowane przez klas�

	u2  m_field_count;						// Ilo�� p�l w klasie
	conv_field_info* m_field_table;			// Tabela p�l w klasie

	u2  m_method_count;						// Ilo�� metod w klasie
	conv_method_info* m_method_table;		// Tabela metod w klasie

	std::string m_class_name;				// Nazwa klasy
};

struct library_class_info
{
	std::string		m_class_file_path;
	std::string		m_class_name;
};
