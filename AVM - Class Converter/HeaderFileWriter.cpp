#include "HeaderFileWriter.h"
#include "defs/defines.h"
#include <vector>
#include <iostream>
#include <fstream>

std::string HeaderFileWriter::AddUnderscoresForClassName(std::string className)
{
	for (int i = 0; i < (int)className.length(); i++)
		if (isupper(className[i]))
			if (i > 0)
				if (islower(className[i-1]))
					className.insert(className.begin() + i, '_');

	return className;
}

void HeaderFileWriter::CreateClassSignatures()
{
	std::vector<u1>* bytecodeStream = new std::vector<u1>();

	bytecodeStream->push_back(be_u2_get_st_byte(this->m_convertedClass->m_this_class_signature));
	bytecodeStream->push_back(be_u2_get_nd_byte(this->m_convertedClass->m_this_class_signature));

	bytecodeStream->push_back(be_u2_get_st_byte(this->m_convertedClass->m_super_class_signature));
	bytecodeStream->push_back(be_u2_get_nd_byte(this->m_convertedClass->m_super_class_signature));

	this->m_reducedBytecodeCount += bytecodeStream->size();

	WriteLine("u1 " + this->m_classNameLower + "_class_signatures[4] PROGMEM = ", this->m_vFileContent);
	WriteLine("{", this->m_vFileContent);
	for (int i = 0; i < 2; i++)
		WriteLine("\t" + Utils::IntToByteHex(bytecodeStream->at(i * 2)) + ", " + Utils::IntToByteHex(bytecodeStream->at((i * 2) + 1)) + ",", this->m_vFileContent);
	WriteLine("};", this->m_vFileContent);
	WriteLine("", this->m_vFileContent);
}

void HeaderFileWriter::CreateClassMethods()
{
	std::vector<u1>* bytecodeStream = new std::vector<u1>();

	bytecodeStream->push_back(be_u2_get_st_byte(this->m_convertedClass->m_method_count));
	bytecodeStream->push_back(be_u2_get_nd_byte(this->m_convertedClass->m_method_count));

	for (int i = 0; i < this->m_convertedClass->m_method_count; i++)
	{
		bytecodeStream->push_back(be_u2_get_st_byte(this->m_convertedClass->m_method_table[i].m_signature));
		bytecodeStream->push_back(be_u2_get_nd_byte(this->m_convertedClass->m_method_table[i].m_signature));

		//bytecodeStream->push_back(be_u4_get_st_byte(this->m_convertedClass->m_method_table[i].m_return_value));
		//bytecodeStream->push_back(be_u4_get_nd_byte(this->m_convertedClass->m_method_table[i].m_return_value));
		//bytecodeStream->push_back(be_u4_get_rd_byte(this->m_convertedClass->m_method_table[i].m_return_value));
		//bytecodeStream->push_back(be_u4_get_th_byte(this->m_convertedClass->m_method_table[i].m_return_value));

		bytecodeStream->push_back(be_u2_get_st_byte(this->m_convertedClass->m_method_table[i].m_parameter_count));
		bytecodeStream->push_back(be_u2_get_nd_byte(this->m_convertedClass->m_method_table[i].m_parameter_count));

		//for (int j = 0; j < this->m_convertedClass->m_method_table[i].m_parameter_count; j++)
		//{
		//	bytecodeStream->push_back(be_u4_get_st_byte(this->m_convertedClass->m_method_table[i].m_parameter_table[j]));
		//	bytecodeStream->push_back(be_u4_get_nd_byte(this->m_convertedClass->m_method_table[i].m_parameter_table[j]));
		//	bytecodeStream->push_back(be_u4_get_rd_byte(this->m_convertedClass->m_method_table[i].m_parameter_table[j]));
		//	bytecodeStream->push_back(be_u4_get_th_byte(this->m_convertedClass->m_method_table[i].m_parameter_table[j]));
		//}

		//bytecodeStream->push_back(be_u2_get_st_byte(this->m_convertedClass->m_method_table[i].m_max_stack));
		//bytecodeStream->push_back(be_u2_get_nd_byte(this->m_convertedClass->m_method_table[i].m_max_stack));

		//bytecodeStream->push_back(be_u2_get_st_byte(this->m_convertedClass->m_method_table[i].m_max_locals));
		//bytecodeStream->push_back(be_u2_get_nd_byte(this->m_convertedClass->m_method_table[i].m_max_locals));

		bytecodeStream->push_back(be_u4_get_st_byte(this->m_convertedClass->m_method_table[i].m_code_length));
		bytecodeStream->push_back(be_u4_get_nd_byte(this->m_convertedClass->m_method_table[i].m_code_length));
		bytecodeStream->push_back(be_u4_get_rd_byte(this->m_convertedClass->m_method_table[i].m_code_length));
		bytecodeStream->push_back(be_u4_get_th_byte(this->m_convertedClass->m_method_table[i].m_code_length));

		for (u4 j = 0; j < this->m_convertedClass->m_method_table[i].m_code_length; j++)
			bytecodeStream->push_back(this->m_convertedClass->m_method_table[i].m_code[j]);
	}

	this->m_reducedBytecodeCount += bytecodeStream->size();

	WriteLine("#define " + this->m_classNameUpper + "_CLASS_METHODS_SIZE\t\t" + Utils::IntToDecStr(bytecodeStream->size()), this->m_vFileContent);
	WriteLine("u1 " + this->m_classNameLower + "_class_methods[" + this->m_classNameUpper + "_CLASS_METHODS_SIZE] PROGMEM = ", this->m_vFileContent);
	WriteLine("{", this->m_vFileContent);
	for (int i = 0; i < (int)bytecodeStream->size(); i++)
	{
		if (!(i % 15)) {
			if (i)
				Write("\r\n", this->m_vFileContent);
			Write("\t", this->m_vFileContent);
		}

		u1 byte = bytecodeStream->at(i);
		Write(Utils::IntToByteHex(byte) + (((i + 1) < (int)bytecodeStream->size()) ? ", " : " "), this->m_vFileContent);
	}
	WriteLine("", this->m_vFileContent);
	WriteLine("};", this->m_vFileContent);
	WriteLine("", this->m_vFileContent);
}

void HeaderFileWriter::CreateClassFields()
{
	std::vector<u1>* bytecodeStream = new std::vector<u1>();

	bytecodeStream->push_back(be_u2_get_st_byte(this->m_convertedClass->m_field_count));
	bytecodeStream->push_back(be_u2_get_nd_byte(this->m_convertedClass->m_field_count));

	for (int i = 0; i < this->m_convertedClass->m_field_count; i++)
	{
		bytecodeStream->push_back(be_u2_get_st_byte(this->m_convertedClass->m_field_table[i].m_signature));
		bytecodeStream->push_back(be_u2_get_nd_byte(this->m_convertedClass->m_field_table[i].m_signature));

		bytecodeStream->push_back(be_u4_get_st_byte(this->m_convertedClass->m_field_table[i].m_type));
		bytecodeStream->push_back(be_u4_get_nd_byte(this->m_convertedClass->m_field_table[i].m_type));
		bytecodeStream->push_back(be_u4_get_rd_byte(this->m_convertedClass->m_field_table[i].m_type));
		bytecodeStream->push_back(be_u4_get_th_byte(this->m_convertedClass->m_field_table[i].m_type));
	}

	this->m_reducedBytecodeCount += bytecodeStream->size();

	WriteLine("#define " + this->m_classNameUpper + "_CLASS_FIELDS_SIZE\t\t" + Utils::IntToDecStr(bytecodeStream->size()), this->m_vFileContent);
	WriteLine("u1 " + this->m_classNameLower + "_class_fields[" + this->m_classNameUpper + "_CLASS_FIELDS_SIZE] PROGMEM = ", this->m_vFileContent);
	WriteLine("{", this->m_vFileContent);
	for (int i = 0; i < (int)bytecodeStream->size(); i++)
	{
		if (!(i % 15)) {
			if (i)
				Write("\r\n", this->m_vFileContent);
			Write("\t", this->m_vFileContent);
		}

		u1 byte = bytecodeStream->at(i);
		Write(Utils::IntToByteHex(byte) + (((i + 1) < (int)bytecodeStream->size()) ? ", " : " "), this->m_vFileContent);
	}
	WriteLine("", this->m_vFileContent);
	WriteLine("};", this->m_vFileContent);
	WriteLine("", this->m_vFileContent);
}

void HeaderFileWriter::CreateClassConstPools()
{
	std::vector<u1>* bytecodeStream = new std::vector<u1>();

	bytecodeStream->push_back(be_u2_get_st_byte(this->m_convertedClass->m_const_pool_count));
	bytecodeStream->push_back(be_u2_get_nd_byte(this->m_convertedClass->m_const_pool_count));

	for (int i = 0; i < this->m_convertedClass->m_const_pool_count - 1; i++)
	{
		bytecodeStream->push_back(this->m_convertedClass->m_const_pool_table[i].m_tag);

		if (!!(this->m_convertedClass->m_const_pool_table[i].m_tag & CONST_POOL_ID_SkipPoolFlag))
			continue;

		for (int j = 0; j < this->m_convertedClass->m_const_pool_table[i].M_size_no_tag; j++)
			bytecodeStream->push_back(this->m_convertedClass->m_const_pool_table[i].m_data[j]);
	}

	this->m_reducedBytecodeCount += bytecodeStream->size();

	WriteLine("#define " + this->m_classNameUpper + "_CLASS_CONST_POOLS_SIZE\t\t" + Utils::IntToDecStr(bytecodeStream->size()), this->m_vFileContent);
	WriteLine("u1 " + this->m_classNameLower + "_class_const_pools[" + this->m_classNameUpper + "_CLASS_CONST_POOLS_SIZE] PROGMEM = ", this->m_vFileContent);
	WriteLine("{", this->m_vFileContent);
	for (int i = 0; i < (int)bytecodeStream->size(); i++)
	{
		if (!(i % 15)) {
			if (i)
				Write("\r\n", this->m_vFileContent);
			Write("\t", this->m_vFileContent);
		}

		u1 byte = bytecodeStream->at(i);
		Write(Utils::IntToByteHex(byte) + (((i + 1) < (int)bytecodeStream->size()) ? ", " : " "), this->m_vFileContent);
	}
	WriteLine("", this->m_vFileContent);
	WriteLine("};", this->m_vFileContent);
	WriteLine("", this->m_vFileContent);
}

void HeaderFileWriter::CreateClassInterfaces()
{
	std::vector<u1>* bytecodeStream = new std::vector<u1>();

	bytecodeStream->push_back(be_u2_get_st_byte(this->m_convertedClass->m_interface_count));
	bytecodeStream->push_back(be_u2_get_nd_byte(this->m_convertedClass->m_interface_count));

	for (int i = 0; i < this->m_convertedClass->m_interface_count; i++)
	{
		bytecodeStream->push_back(be_u2_get_st_byte(this->m_convertedClass->m_interface_table[i]));
		bytecodeStream->push_back(be_u2_get_nd_byte(this->m_convertedClass->m_interface_table[i]));
	}

	this->m_reducedBytecodeCount += bytecodeStream->size();

	WriteLine("#define " + this->m_classNameUpper + "_CLASS_INTERFACES_SIZE\t\t" + Utils::IntToDecStr(bytecodeStream->size()), this->m_vFileContent);
	WriteLine("u1 " + this->m_classNameLower + "_class_interfaces[" + this->m_classNameUpper + "_CLASS_INTERFACES_SIZE] PROGMEM = ", this->m_vFileContent);
	WriteLine("{", this->m_vFileContent);
	for (int i = 0; i < (int)bytecodeStream->size(); i++)
	{
		if (!(i % 2)) {
			if (i)
				Write("\r\n", this->m_vFileContent);
			Write("\t", this->m_vFileContent);
		}

		u1 byte = bytecodeStream->at(i);
		Write(Utils::IntToByteHex(byte) + (((i + 1) < (int)bytecodeStream->size()) ? ", " : " "), this->m_vFileContent);
	}
	WriteLine("", this->m_vFileContent);
	WriteLine("};", this->m_vFileContent);
	WriteLine("", this->m_vFileContent);
}

void HeaderFileWriter::CreateCommonFile()
{
	this->m_vFileContent->clear();

	WriteLine("#ifndef __HEADER__CLASSINFO", this->m_vFileContent);
	WriteLine("#define __HEADER__CLASSINFO", this->m_vFileContent);
	WriteLine("", this->m_vFileContent);
	
	
	WriteLine("/// ============= CLASS FILES BYTECODE ================ ///", this->m_vFileContent);
	WriteLine("", this->m_vFileContent);
	for (int i = 0; i < (int)this->m_vClassNames->size(); i++)
		WriteLine("#include \"" + this->m_vClassNames->at(i) + ".h\"", this->m_vFileContent);
	WriteLine("", this->m_vFileContent);
	WriteLine("/// =================================================== ///", this->m_vFileContent);
	
	
	WriteLine("", this->m_vFileContent);
	WriteLine("#define TOTAL_CLASSES\t\t" + Utils::IntToDecStr(this->m_vClassNames->size()), this->m_vFileContent);

	WriteLine("", this->m_vFileContent);
	WriteLine("/// SIGNATURES ///", this->m_vFileContent);
	WriteLine("", this->m_vFileContent);

	WriteLine("u1* ClassesSignatures[TOTAL_CLASSES] = ", this->m_vFileContent);
	WriteLine("{", this->m_vFileContent);
	for (int i = 0; i < (int)this->m_vClassNames->size(); i++)
		WriteLine("\t" + this->m_vClassNames->at(i) + "_class_signatures,", this->m_vFileContent);
	WriteLine("};", this->m_vFileContent);
	
	WriteLine("", this->m_vFileContent);
	WriteLine("/// METHODS ///", this->m_vFileContent);
	WriteLine("", this->m_vFileContent);

	WriteLine("u1* ClassesMethods[TOTAL_CLASSES] = ", this->m_vFileContent);
	WriteLine("{", this->m_vFileContent);
	for (int i = 0; i < (int)this->m_vClassNames->size(); i++)
		WriteLine("\t" + this->m_vClassNames->at(i) + "_class_methods,", this->m_vFileContent);
	WriteLine("};", this->m_vFileContent);
	
	WriteLine("", this->m_vFileContent);

	WriteLine("int ClassesMethodsSizes[TOTAL_CLASSES] = ", this->m_vFileContent);
	WriteLine("{", this->m_vFileContent);
	for (int i = 0; i < (int)this->m_vClassNames->size(); i++)
		WriteLine("\t" + Utils::ToUpper(this->m_vClassNames->at(i)) + "_CLASS_METHODS_SIZE,", this->m_vFileContent);
	WriteLine("};", this->m_vFileContent);

	WriteLine("", this->m_vFileContent);
	WriteLine("/// FIELDS ///", this->m_vFileContent);
	WriteLine("", this->m_vFileContent);

	WriteLine("u1* ClassesFields[TOTAL_CLASSES] = ", this->m_vFileContent);
	WriteLine("{", this->m_vFileContent);
	for (int i = 0; i < (int)this->m_vClassNames->size(); i++)
		WriteLine("\t" + this->m_vClassNames->at(i) + "_class_fields,", this->m_vFileContent);
	WriteLine("};", this->m_vFileContent);
	
	WriteLine("", this->m_vFileContent);

	WriteLine("int ClassesFieldsSizes[TOTAL_CLASSES] = ", this->m_vFileContent);
	WriteLine("{", this->m_vFileContent);
	for (int i = 0; i < (int)this->m_vClassNames->size(); i++)
		WriteLine("\t" + Utils::ToUpper(this->m_vClassNames->at(i)) + "_CLASS_FIELDS_SIZE,", this->m_vFileContent);
	WriteLine("};", this->m_vFileContent);

	WriteLine("", this->m_vFileContent);
	WriteLine("/// CONST POOLS ///", this->m_vFileContent);
	WriteLine("", this->m_vFileContent);

	WriteLine("u1* ClassesConstPools[TOTAL_CLASSES] = ", this->m_vFileContent);
	WriteLine("{", this->m_vFileContent);
	for (int i = 0; i < (int)this->m_vClassNames->size(); i++)
		WriteLine("\t" + this->m_vClassNames->at(i) + "_class_const_pools,", this->m_vFileContent);
	WriteLine("};", this->m_vFileContent);

	WriteLine("", this->m_vFileContent);
	WriteLine("/// INTERFACES ///", this->m_vFileContent);
	WriteLine("", this->m_vFileContent);

	WriteLine("u1* ClassesInterfaces[TOTAL_CLASSES] = ", this->m_vFileContent);
	WriteLine("{", this->m_vFileContent);
	for (int i = 0; i < (int)this->m_vClassNames->size(); i++)
		WriteLine("\t" + this->m_vClassNames->at(i) + "_class_interfaces,", this->m_vFileContent);
	WriteLine("};", this->m_vFileContent);

	WriteLine("", this->m_vFileContent);

	WriteLine("#endif", this->m_vFileContent);

	if (this->m_fileContent) {
		delete[] this->m_fileContent;
	}

	this->m_fileContent = new char[this->m_vFileContent->size()];
	for (int i = 0; i < (int)this->m_vFileContent->size(); i++)
		this->m_fileContent[i] = this->m_vFileContent->at(i);
	this->m_fileContentCount = this->m_vFileContent->size();


	std::ofstream outputFile("Output/class_info.h", std::ios::binary);

	if (outputFile.is_open()) {
		for (int i = 0; i < this->m_fileContentCount; i++)
			outputFile << this->m_fileContent[i];
		outputFile.close();
	}

	m_vClassNames->clear();
}


void HeaderFileWriter::LoadConvertedClass(converted_java_class_file_format * convertedClass)
{
	// nazwa klasy z formatu package/Class
	std::vector<std::string> classNameSplit = Utils::Split(convertedClass->m_class_name, '/');
	std::string className = classNameSplit.at(classNameSplit.size() - 1);

	// jezeli nazwa klasy zawiera znak $ np. AtMega8$ISRVectors
	// to zamien $ na _ czyli AtMega_ISRVectors -> atmega8_isrvectors

	this->m_className = className;

	if (className.find('$') != std::string::npos)
	{
		std::vector<std::string> classNameParts = Utils::Split(className, '$');
		for (int i = 0; i < (int)classNameParts.size(); i++)
			classNameParts[i] = AddUnderscoresForClassName(classNameParts.at(i));
		className = Utils::Join(classNameParts, "_");
	}
	else {
		// jezeli nie ma takiego znaku dodaj _ przez duza litera
		className = AddUnderscoresForClassName(className);
	}

	this->m_convertedClass = convertedClass;

	if (this->m_vFileContent) {
		this->m_vFileContent->clear();
		delete this->m_vFileContent;
	}
	this->m_vFileContent = new std::vector<char>();

	this->m_classNameUpper = Utils::ToUpper(className);
	this->m_classNameLower = Utils::ToLower(className);
}

void HeaderFileWriter::Write(std::string line, std::vector<char>* vChars)
{
	const char* c_str = line.c_str();
	for (int i = 0; c_str[i]; i++)
		vChars->push_back(c_str[i]);
}

void HeaderFileWriter::WriteLine(std::string line, std::vector<char>* vChars)
{

	Write(line, vChars);
	vChars->push_back('\r'); vChars->push_back('\n');
}

void HeaderFileWriter::CreateHeaderFile()
{
	this->m_reducedBytecodeCount = 0;

	WriteLine("#ifndef __HEADER__CLASS_" + this->m_classNameUpper, this->m_vFileContent);
	WriteLine("#define __HEADER__CLASS_" + this->m_classNameUpper, this->m_vFileContent);
	WriteLine("", this->m_vFileContent);
	WriteLine("#include <avr/pgmspace.h>", this->m_vFileContent);
	WriteLine("", this->m_vFileContent);
	CreateClassSignatures();
	CreateClassMethods();
	CreateClassFields();
	CreateClassConstPools();
	CreateClassInterfaces();
	WriteLine("", this->m_vFileContent);
	WriteLine("#endif", this->m_vFileContent);

	if (this->m_fileContent) {
		delete[] this->m_fileContent;
	}

	this->m_fileContent = new char[this->m_vFileContent->size()];
	for (int i = 0; i < (int)this->m_vFileContent->size(); i++)
		this->m_fileContent[i] = this->m_vFileContent->at(i);
	this->m_fileContentCount = this->m_vFileContent->size();
}

void HeaderFileWriter::WriteFile()
{
	std::string fileLocation = this->m_outputDirectoryPath + "/Output";
	std::string fileName = this->m_classNameLower + ".h";
	std::string filePath = fileLocation + "/" + fileName;
	Utils::RecursiveCreateDirectory(fileLocation);

	m_vClassNames->push_back(this->m_classNameLower);

	std::ofstream outputFile(filePath, std::ios::binary);

	if (outputFile.is_open()) {
		for (int i = 0; i < this->m_fileContentCount; i++)
			outputFile << this->m_fileContent[i];
		outputFile.close();
	}

	std::cout << "Saved file to: " << filePath << std::endl;

	std::cout << "Class: " << this->m_className << ", saved to file: " << fileName << ", total bytecode count: " << this->m_reducedBytecodeCount << " bytes" << std::endl;
}

void HeaderFileWriter::SetOutputDirectoryPath(std::string outputDirectoryPath)
{
	this->m_outputDirectoryPath = outputDirectoryPath;
}