#include <iostream>
#include <string>
#include <iomanip>

#include "defs/stdtypes.h"

#include "defs/defines.h"

#include "defs/appinfo.h"
#include "Utils.h"

#include "ConsoleArguments.h"
#include "BytecodeReader.h"
#include "JavaClassLoader.h"
#include "JavaClassReducer.h"
#include "HeaderFileWriter.h"
#include "JavaClassMapper.h"
#include "JavaClassLibraryReducer.h"
#include "JavaClassPrinter.h"
#include "JavaClassUtils.h"
#include "JavaClassReferenceLoader.h"
#include "Utils.h"

#define RES_MAIN_BASE									(0x00)
#define  RES_MAIN_CONVERT_CLASSES_OK					 (RES_MAIN_BASE|0x00)
#define  RES_MAIN_CONVERT_CLASSES_NO_ANY_LIB_CLASSES	 (RES_MAIN_BASE|0x01)
#define  RES_MAIN_CONVERT_CLASSES_NO_ANY_USR_CLASSES	 (RES_MAIN_BASE|0x02)
#define  RES_MAIN_CONVERT_CLASSES_LOADING_CLASS_ERROR	 (RES_MAIN_BASE|0x03)
#define  RES_MAIN_CONVERT_CLASSES_REFERENCE_LOAD_ERROR	 (RES_MAIN_BASE|0x04)
#define  RES_MAIN_CONVERT_CLASSES_REDUCER_ERROR			 (RES_MAIN_BASE|0x05)

std::string Utils::ExecutableFilePath = "";

int main_fake(int argc, char** argv);
int main(int argc, char** argv);

int convert_classes(std::string userClassesRootPath, std::string libraryClassesRootPath, bool verboseMessages, bool createMapFiles);
void show_class_info(std::string inputClassFile, bool showBytecode);
void create_class_map(std::string inputClassFileA, std::string inputClassFileB, bool makeDiffFile);

int main(int argc, char** argv)
{
	srand((unsigned int)time(NULL));
	Utils::SetExecutableFilePath(STR(argv[0]));

	ConsoleArguments* consoleArguments = new ConsoleArguments(argc, argv, std::cout);
	consoleArguments->PrintInfo();
	
	if (consoleArguments->ArgsExists())
	{
		int errorCode = 0;
		consoleArguments->PrintCommandLine();
		if ((errorCode = consoleArguments->ParseArgs()) == RES_CONSOLE_ARG_PARSE_OK)
		{
			int result = 0;
			switch (consoleArguments->GetMode())
			{
			case CONSOLE_ARG_MODE_CONVERT_CLASSES:
				std::cout << "Running mode: convert classes" << std::endl;
				if ((result = convert_classes(consoleArguments->ParseGetSourceDirectory(), consoleArguments->ParseGetLibDirectory(),
					!!(consoleArguments->GetOptionNumber() & CONSOLE_ARG_OPTION_VERBOSE), !!(consoleArguments->GetOptionNumber() & CONSOLE_ARG_OPTION_CREATE_MAP_FILES)) == RES_MAIN_CONVERT_CLASSES_OK))
				{
					std::cout << "Converion done! Paste created files into classes directory in VM" << std::endl;
					return EXIT_SUCCESS;
				}
				else {
					std::cout << "Error occured, consider following error code: " << Utils::IntToWordHex(result) << std::endl;
					return EXIT_FAILURE;
				}
				break;

			case CONSOLE_ARG_MODE_SHOW_CLASS_INFO:
				std::cout << "Running mode: show class info" << std::endl;
				show_class_info(consoleArguments->InfoGetInputFile(), !!(consoleArguments->GetOptionNumber() & CONSOLE_ARG_OPTION_SHOW_BYTECODE));
				break;

			case CONSOLE_ARG_MODE_CREATE_CLASS_MAP:
				std::cout << "Running mode: create map file" << std::endl;
				create_class_map(consoleArguments->MapGetInputFileA(), consoleArguments->MapGetInputFileB(), !!(consoleArguments->GetOptionNumber() & CONSOLE_ARG_OPTION_MAP_DIFF));
				break;
			case CONSOLE_ARG_MODE_SHOW_HELP:
				consoleArguments->PrintHelp();
				break;
			}
		}
		else
		{
			std::cout << "Error: " << consoleArguments->ErrorInfo(errorCode) << std::endl;
			consoleArguments->PrintHelp();
		}
	}
	else
	{
		consoleArguments->PrintHelp();
	}

	return 0;
}


int convert_classes(std::string userClassesRootPath, std::string libraryClassesRootPath, bool verboseMessages, bool createMapFiles)
{
	int error = RES_MAIN_CONVERT_CLASSES_OK;

	/* Skanowanie katalogu klas użytkownika */
	std::cout << std::endl;
	std::cout << "Scanning user classes root directory" << std::endl;
	std::vector<std::string> userClassFiles = Utils::ScanDirectoryRecursive(userClassesRootPath + "\\*.class");
	if (!userClassFiles.size()) {
		std::cout << "Couldn't find any AVR library class" << std::endl;
		return error = RES_MAIN_CONVERT_CLASSES_NO_ANY_USR_CLASSES;
	}
	std::cout << "Found user classes:" << std::endl;
	for (int i = 0; i < (int)userClassFiles.size(); i++)
		std::cout << (i + 1) << ". " << userClassFiles.at(i) << std::endl;

	std::cout << std::endl;

	/* Skanowanie katalogu klas biblioteki Java AVR */
	std::cout << "Scanning AVR Java library root directory" << std::endl;
	std::vector<std::string> libraryClassFiles = Utils::ScanDirectoryRecursive(libraryClassesRootPath + "\\*.class");
	if (!libraryClassFiles.size()) {
		std::cout << "Couldn't find any AVR library class" << std::endl;
		return error = RES_MAIN_CONVERT_CLASSES_NO_ANY_LIB_CLASSES;
	}
	std::cout << "Found library classes:" << std::endl;
	for (int i = 0; i < (int)libraryClassFiles.size(); i++)
		std::cout << (i + 1) << ". " << libraryClassFiles.at(i) << std::endl;

	/* Tworzenie struktur i listy z nazwami klas z bilioteki  */
	std::vector<library_class_info>* libraryClassesInfo = new std::vector<library_class_info>();
	std::vector<std::string> libraryClassNames = std::vector<std::string>();
	for (int i = 0; i < (int)libraryClassFiles.size(); i++)
	{
		std::string libraryClassFilePath = libraryClassFiles.at(i);

		// Nazwa klasy z biblioteki
		std::string className = libraryClassFilePath.substr(libraryClassesRootPath.length() + 1, libraryClassFilePath.length() - libraryClassesRootPath.length());
		className = className.substr(0, className.find('.'));
		libraryClassNames.push_back(className);

		// Struktura klasy z biblioteki
		library_class_info libraryClass = {};
		libraryClass.m_class_file_path = libraryClassFilePath;
		libraryClass.m_class_name = className;
		libraryClassesInfo->push_back(libraryClass);
	}

	std::cout << std::endl;

	/* Ładowanie wszystkich klas użytkownika */
	standard_java_class_file_format* userClassFormats = new standard_java_class_file_format[userClassFiles.size()];
	std::cout << "Loading all user classes:" << std::endl;
	for (int i = 0; i < (int)userClassFiles.size(); i++)
	{
		std::string userClassFilePath = userClassFiles.at(i);
		std::cout << "Loading class: " << Utils::GetFileNameFromPath(userClassFilePath) << " ";
		if ((error = JavaClassUtils::GetClassStandardFileFormatFromFile(userClassFilePath, &userClassFormats[i])) != RES_CLASS_LOAD_OK)
		{
			std::cout << "An error occured while loading class" << std::endl;
			return (RES_MAIN_CONVERT_CLASSES_LOADING_CLASS_ERROR << 8) | error;
		}
		else {
			std::cout << "Done" << std::endl;
		}
	}

	std::cout << std::endl;

	/* Szukanie referencji w klasie użytkownika */
	std::cout << "Searching for library class references in user classes: ";

	JavaClassReferenceLoader* javaClassReferenceLoader = new JavaClassReferenceLoader();
	for (int i = 0; i < (int)userClassFiles.size(); i++)
		javaClassReferenceLoader->AddUserClass(&userClassFormats[i]);

	javaClassReferenceLoader->SetLibraryRootPath(libraryClassesRootPath);
	if ((error = javaClassReferenceLoader->FindReferences()) != RES_CLASS_REFERENCE_LOADER_OK) {
		std::cout << "An error occured while searching for class references" << std::endl;
		return (RES_MAIN_CONVERT_CLASSES_REFERENCE_LOAD_ERROR << 8) | error;
	}
	else {
		std::cout << "Done" << std::endl;
	}

	standard_java_class_file_format* allUsedLibraryClassFormats = 0;
	int allUsedLibraryClassFormatsCount = 0;
	javaClassReferenceLoader->GetFoundReferences(allUsedLibraryClassFormats, allUsedLibraryClassFormatsCount);

	/* Konwersja i redukcja danych wszystkich załadowanych klas */
	std::cout << "Reducing all loaded classes: ";
	JavaClassReducer* javaClassReducer = new JavaClassReducer();

	// Dodawanie klas użytkownika
	for (int i = 0; i < (int)userClassFiles.size(); i++)
		javaClassReducer->AddClass(userClassFormats[i]);

	// Dodawanie klas z biblioteki AVR
	for (int i = 0; i < allUsedLibraryClassFormatsCount; i++)
		javaClassReducer->AddClass(allUsedLibraryClassFormats[i]);

	if ((error = javaClassReducer->ReduceClasses()) != RES_CLASS_REDUCER_OK) {
		std::cout << "An error occured while reducing class" << std::endl;
		return (RES_MAIN_CONVERT_CLASSES_REDUCER_ERROR << 8) | error;
	}
	else {
		std::cout << "Done" << std::endl;
	}

	converted_java_class_file_format* reducedClasses = 0;
	int reducedClassesCount = 0;
	javaClassReducer->GetReducedClasses(reducedClasses, &reducedClassesCount);

	std::cout << std::endl;

	/* Tworzenie plików nagłówkowych .h */
	std::cout << "Creating C header files" << std::endl;
	HeaderFileWriter* headerFileWriter = new HeaderFileWriter();
	headerFileWriter->SetOutputDirectoryPath(Utils::GetExeCurrentDirectory());

	for (int i = 0; i < reducedClassesCount; i++)
	{
		headerFileWriter->LoadConvertedClass(&reducedClasses[i]);
		headerFileWriter->CreateHeaderFile();
		headerFileWriter->WriteFile();
	}

	headerFileWriter->CreateCommonFile();

	if (createMapFiles) {
		JavaClassMapper* javaMapper = new JavaClassMapper();
		javaMapper->SetOutputDirectoryPath(Utils::GetExeCurrentDirectory());

		for (int i = 0; i < reducedClassesCount; i++)
		{
			javaMapper->CreateConvertedClassMap(reducedClasses[i]);
			std::vector<std::string> splitClassName = Utils::Split(reducedClasses[i].m_class_name, '/');
			if (splitClassName.size())
				javaMapper->DumpMapFile(splitClassName.at(splitClassName.size() - 1));
		}

		for (int i = 0; i < (int)userClassFiles.size(); i++)
		{
			javaMapper->CreateStandardClassMap(userClassFormats[i]);
			std::vector<std::string> splitClassName = Utils::Split(userClassFormats[i].m_class_name, '/');
			if (splitClassName.size())
				javaMapper->DumpMapFile(splitClassName.at(splitClassName.size() - 1));
		}

		for (int i = 0; i < allUsedLibraryClassFormatsCount; i++)
		{
			javaMapper->CreateStandardClassMap(allUsedLibraryClassFormats[i]);
			std::vector<std::string> splitClassName = Utils::Split(allUsedLibraryClassFormats[i].m_class_name, '/');
			if (splitClassName.size())
				javaMapper->DumpMapFile(splitClassName.at(splitClassName.size() - 1));
		}

		std::cout << std::endl;
	}
	return error = RES_MAIN_CONVERT_CLASSES_OK;
}

void show_class_info(std::string inputClassFile, bool showBytecode)
{
	u1* bytecode = 0;
	int bytecodeSize = 0;

	BytecodeReader* bytecodeReader = new BytecodeReader();
	bytecodeReader->ReadBytecode(inputClassFile);
	bytecodeReader->GetBytecode(bytecode, bytecodeSize);

	standard_java_class_file_format classFormat = {};
	JavaClassUtils::GetClassStandardFileFormatFromFile(inputClassFile, &classFormat);

	JavaClassPrinter::PrintClassData(&classFormat);

	if (showBytecode) {
		std::cout << std::endl;
		std::cout << "Class bytecode" << std::endl;
		std::cout << std::endl;

		for (int i = 0; i < bytecodeSize; i++) {
			if (!(i % 20)) {
				if (i)
					std::cout << std::endl;
				std::cout << "\t";
			}

			std::cout << Utils::IntToByteHex(bytecode[i]) << " ";
		}
		std::cout << std::endl;
	}
}

void create_class_map(std::string inputClassFileA, std::string inputClassFileB, bool makeDiffFile)
{
	standard_java_class_file_format classFormat = {};
	JavaClassUtils::GetClassStandardFileFormatFromFile(inputClassFileA, &classFormat);
	std::string fileRealName = Utils::GetFileRealName(Utils::GetFileNameFromPath(inputClassFileA));

	JavaClassMapper* javaClassMapper = new JavaClassMapper();
	javaClassMapper->CreateStandardClassMap(classFormat);
	javaClassMapper->SetOutputDirectoryPath(Utils::GetExeCurrentDirectory());
	javaClassMapper->DumpMapFile(fileRealName);
}