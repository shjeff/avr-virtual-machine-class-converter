#pragma once
#include <vector>
#include <string>
#include "JavaClassUtils.h"
#include "Utils.h"

#define RES_CLASS_REFERENCE_LOADER_BASE				(0x40)
#define RES_CLASS_REFERENCE_LOADER_OK				(RES_CLASS_REFERENCE_LOADER_BASE|0x00)

class JavaClassReferenceLoader
{
private:
	std::vector<standard_java_class_file_format*>* m_userClasses;
	std::string m_libraryRootPath;
	std::vector<library_class_info>* m_foundClassReferences;
	int m_error;

public:
	void JavaClassReferenceLoader::AddUserClass(standard_java_class_file_format * userClass);
	void JavaClassReferenceLoader::SetLibraryRootPath(std::string javaLibraryRootPath);
	int  JavaClassReferenceLoader::FindReferences(void);
	void JavaClassReferenceLoader::GetFoundReferences(standard_java_class_file_format *& referenceClasses, int & referenceClassesCount);

	JavaClassReferenceLoader(void):
		m_userClasses(new std::vector<standard_java_class_file_format*>()),
		m_foundClassReferences(new std::vector<library_class_info>()),
		m_error(0) {  }

	~JavaClassReferenceLoader(void) {  }
};

