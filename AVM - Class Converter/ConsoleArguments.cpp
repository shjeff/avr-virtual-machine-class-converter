#include "ConsoleArguments.h"
#include "defs/appinfo.h"
#include "defs/defines.h"
#include "Utils.h"
#include <string>

int ConsoleArguments::ParseArgs(void)
{
	this->m_runMode = 0;

	for (int i = 1; i < this->m_argc; i++)
	{
		if (i == 1)
		{
			std::string action = STR(this->m_argv[i]);

			// Pierwszy argument programu oznacza tryb pracy
			if (action == "transform") {
				this->m_runMode = CONSOLE_ARG_MODE_CONVERT_CLASSES;
			}
			else if (action == "show") {
				this->m_runMode = CONSOLE_ARG_MODE_SHOW_CLASS_INFO;
			}
			else if (action == "map") {
				this->m_runMode = CONSOLE_ARG_MODE_CREATE_CLASS_MAP;
			}
			else if (action == "-h" || action == "help") {
				this->m_runMode = CONSOLE_ARG_MODE_SHOW_HELP;
				break;
			}
			else {
				return RES_CONSOLE_ARG_PARSE_ERROR_UNKNOWN_OPTION;
			}
		}
		else
		{
			switch (this->m_runMode)
			{
				case CONSOLE_ARG_MODE_CONVERT_CLASSES:
				{
					std::string option = STR(this->m_argv[i]);

					if (option == "-v") {
						this->m_optionNumber |= CONSOLE_ARG_OPTION_VERBOSE;
					}
					else if (option == "-m") {
						this->m_optionNumber |= CONSOLE_ARG_OPTION_CREATE_MAP_FILES;
					}
					else {
						std::string filePath = Utils::GetAbsoluteFilePath(option);
						if (!Utils::IsFileExists(filePath))
							return RES_CONSOLE_ARG_PARSE_ERROR_INPUT_FILE_DOESNT_EXISTS;

						if (Utils::IsFileRegular(filePath))
							return RES_CONSOLE_ARG_PARSE_ERROR_INPUT_FILE_IS_REGULAR;

						if (this->m_rootUserClassesDirectoryPath == "")
							this->m_rootUserClassesDirectoryPath = filePath;
						else
							this->m_rootLibraryClassesDirectoryPath = filePath;
					}
					break;
				}

				case CONSOLE_ARG_MODE_SHOW_CLASS_INFO:
				{
					std::string option = STR(this->m_argv[i]);

					if (option == "-b") {
						this->m_optionNumber |= CONSOLE_ARG_OPTION_SHOW_BYTECODE;
					} else {
						std::string filePath = Utils::GetAbsoluteFilePath(option);
						if (!Utils::IsFileExists(filePath))
							return RES_CONSOLE_ARG_PARSE_ERROR_INPUT_FILE_DOESNT_EXISTS;

						if (!Utils::IsFileRegular(filePath))
							return RES_CONSOLE_ARG_PARSE_ERROR_INPUT_FILE_IS_DIRECTORY;

						this->m_classFilePath = filePath;
					}

					break;
				}

				case CONSOLE_ARG_MODE_CREATE_CLASS_MAP:
				{
					std::string filePath = Utils::GetAbsoluteFilePath(STR(this->m_argv[i]));
					if (!Utils::IsFileExists(filePath))
						return RES_CONSOLE_ARG_PARSE_ERROR_INPUT_FILE_DOESNT_EXISTS;

					if (!Utils::IsFileRegular(filePath))
						return RES_CONSOLE_ARG_PARSE_ERROR_INPUT_FILE_IS_DIRECTORY;

					if (this->m_classFilePathA == "") {
						this->m_classFilePathA = filePath;
					}
					else {
						this->m_classFilePathB = filePath;
						this->m_optionNumber |= CONSOLE_ARG_OPTION_MAP_DIFF;
					}

					break;
				}

				default: return RES_CONSOLE_ARG_PARSE_ERROR_UNKNOWN_OPTION;
			}
		}
	}

	if (this->m_runMode != 0 && this->m_argc == 2)
		return RES_CONSOLE_ARG_PARSE_ERROR_MISSING_PARAMETERS;

	return RES_CONSOLE_ARG_PARSE_OK;
}

void ConsoleArguments::PrintHelp(void)
{
	this->m_outStream << "" << std::endl;
	this->m_outStream << PROG_DETAILED_DESCRIPTION << std::endl;
	this->m_outStream << std::endl;
	this->m_outStream << "Usage:" << std::endl;
	this->m_outStream << "Convert user and library classes and create C header files:" << std::endl;
	this->m_outStream << "      [" << PROG_NAME << "] transform <root-user-classes-directory-path> <root-library-classes-directory-path> [--v] [--m]" << std::endl;
	this->m_outStream << std::endl;
	this->m_outStream << "Show class info:" << std::endl;
	this->m_outStream << "      [" << PROG_NAME << "] show <input-file> [--b]" << std::endl;
	this->m_outStream << std::endl;
	this->m_outStream << "Show map info:" << std::endl;
	this->m_outStream << "      [" << PROG_NAME << "] map <input-file> [<input-file>]" << std::endl;
	this->m_outStream << std::endl;
	this->m_outStream << "Arguments for converting classes:" << std::endl;
	this->m_outStream << "      <root-user-classes-directory-path> - path to compiled user program Java class files" << std::endl;
	this->m_outStream << "      <root-library-classes-directory-path> - path to compiled AVR Java library class files" << std::endl;
	this->m_outStream << "      v - (optional) enable verbose messages" << std::endl;
	this->m_outStream << "      m - (optional) create map files" << std::endl;
	this->m_outStream << std::endl;
	this->m_outStream << "Arguments for showing class info:" << std::endl;
	this->m_outStream << "      <input-file> - path to Java compiled .class file" << std::endl;
	this->m_outStream << "      b - (optional) show bytecode of class" << std::endl;
	this->m_outStream << std::endl;
	this->m_outStream << "Arguments for creating map file:" << std::endl;
	this->m_outStream << "      <input-file> - path to .class file" << std::endl;
	this->m_outStream << "      [<input-file>] - (optional) path to compare .class file" << std::endl;
}

bool ConsoleArguments::ArgsExists(void)
{
	return this->m_argc > 1;
}

void ConsoleArguments::PrintInfo(void)
{
	this->m_outStream << PROG_SHORT_DESCRIPTION << std::endl;
	this->m_outStream << PROG_LEGAL << " " << PROG_VERSION << " " << PROG_AUTHOR << std::endl;
}

int ConsoleArguments::GetMode(void)
{
	return this->m_runMode;
}

std::string ConsoleArguments::MapGetInputFileA(void)
{
	return this->m_classFilePathA;
}

std::string ConsoleArguments::MapGetInputFileB(void)
{
	return this->m_classFilePathB;
}

std::string ConsoleArguments::InfoGetInputFile(void)
{
	return this->m_classFilePath;
}

std::string ConsoleArguments::ParseGetSourceDirectory(void)
{
	return this->m_rootUserClassesDirectoryPath;
}

std::string ConsoleArguments::ParseGetLibDirectory(void)
{
	return this->m_rootLibraryClassesDirectoryPath;
}

int ConsoleArguments::GetOptionNumber(void)
{
	return this->m_optionNumber;
}

std::string ConsoleArguments::ErrorInfo(int error)
{
	switch (error)
	{
		case RES_CONSOLE_ARG_PARSE_ERROR_INPUT_FILE_DOESNT_EXISTS:
			return "Specified file doesn't exists";

		case RES_CONSOLE_ARG_PARSE_ERROR_INPUT_FILE_IS_REGULAR:
			return "Specified file is regular";

		case RES_CONSOLE_ARG_PARSE_ERROR_INPUT_FILE_IS_DIRECTORY:
			return "Specified file is directory";

		case RES_CONSOLE_ARG_PARSE_ERROR_UNKNOWN_OPTION:
			return "Invalid option";

		case RES_CONSOLE_ARG_PARSE_ERROR_MISSING_PARAMETERS:
			return "Missing parameter";

		default:
			return "unknown";
	}
}

void ConsoleArguments::PrintCommandLine(void)
{
	std::vector<std::string>* commandLine = new std::vector<std::string>();
	for (int i = 1; i < this->m_argc; i++)
		commandLine->push_back(STR(this->m_argv[i]));
	
	this->m_outStream << "> " << PROG_NAME << " " << Utils::Join(*commandLine, " ") << std::endl;
}