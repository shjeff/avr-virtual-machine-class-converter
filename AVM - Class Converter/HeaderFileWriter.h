#pragma once
#include <string>
#include <vector>
#include "Utils.h"
#include "defs/javaclass.h"
#include "defs/stdtypes.h"

class HeaderFileWriter
{
private:
	u1* m_reducedBytecode;
	int m_reducedBytecodeCount;
	char* m_fileContent;
	int m_fileContentCount;
	std::string m_outputDirectoryPath;
	std::string m_className;
	std::string m_classNameLower;
	std::string m_classNameUpper;
	converted_java_class_file_format* m_convertedClass;
	std::vector<char>* m_vFileContent;
	std::vector<std::string>* m_vClassNames;


	void HeaderFileWriter::WriteLine(std::string line, std::vector<char>* vChars);
	void HeaderFileWriter::Write(std::string line, std::vector<char>* vChars);
	void HeaderFileWriter::CreateClassSignatures();
	void HeaderFileWriter::CreateClassMethods();
	void HeaderFileWriter::CreateClassFields();
	void HeaderFileWriter::CreateClassConstPools();
	void HeaderFileWriter::CreateClassInterfaces();

	std::string HeaderFileWriter::AddUnderscoresForClassName(std::string className);

public:
	HeaderFileWriter(void):
		m_vClassNames(new std::vector<std::string>())
			{}
	~HeaderFileWriter(void) { }

	void HeaderFileWriter::LoadConvertedClass(converted_java_class_file_format * convertedClass);
	void HeaderFileWriter::CreateHeaderFile();
	void HeaderFileWriter::CreateCommonFile();
	void HeaderFileWriter::WriteFile();
	void HeaderFileWriter::SetOutputDirectoryPath(std::string outputDirectoryPath);
};

